<?php
namespace T3kk\NjBootstrap\DataProcessing;
use T3kk\NjBootstrap\DatabaseService;
use T3kk\NjBootstrap\GeneralService;
use T3kk\NjBootstrap\Service\ImageService;
use T3kk\NjBootstrap\Domain\Repository\IconRepository;
use T3kk\NjBootstrap\Domain\Repository\MarkerRepository;
use T3kk\NjBootstrap\Domain\Repository\SlideRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * Class for data processing for the custom content elements
 * @author Nico Jatzkowski <nico@nj81.de>
 * @package T3kk
 * @subpackage nj_bootstrap
 */
class CustomContentProcessor extends \T3kk\NjBootstrap\DataProcessing\AbstractProcessor
{
	/**
	 * @var array 
	 */
	protected $data = [];
	
	/**
     * @var \TYPO3\CMS\Core\Resource\FileRepository
     */
    protected $fileRepository;
	
	/**
	 * @var \T3kk\NjBootstrap\Domain\Repository\SlideRepository 
	 */
	protected $slideRepository;
	
	/**
	 * @var string 
	 */
	protected $table = 'tt_content';
	
	/**
	 * @var string
	 */
	protected $field = 'image';
	
	/**
	 * @var array 
	 */
	protected $processedData = [];
	
	
	/**
	 * Process data for the content element "My new content element"
     *
     * @param ContentObjectRenderer $cObj The data of the content element or page
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     * @return array the processed data as key/value store
     */
	public function process(
		ContentObjectRenderer $cObj,
		array $contentObjectConfiguration,
		array $processorConfiguration,
		array $processedData
	) {
		$this->data = $cObj->data;
		$this->processedData = $processedData;
		
//		if(is_array($cObj->data) && $cObj->data['uid'] && $this->field) {
//            $this->fileRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\FileRepository');
//            $images = $this->fileRepository->findByRelation($this->table, $this->field, $cObj->data['uid']);
//        } else{
//            $images = NULL;
//        }
//	   
//		if(is_array($images)) {
//			shuffle($images);
//			$this->processedData['image'] = $images[0];
//		}
		
		$this->setAttributes();
		
		return $this->processedData;
	}
	
	
	protected function setImage() {
		
	}
	
	protected function setCssClass() {
		
	}
	
	protected function setAttributes() {
		switch ($this->data['CType']) {
			case 'nj_carousel':
				$this->setAttributesNjCarousel();
				break;
			case 'nj_hero':
				$this->setAttributesNjHero();
				break;
			case 'nj_icons':
				$this->setAttributesNjIcons();
				break;
			case 'nj_maps':
				$this->setAttributesNjMaps();
				break;
			case 'nj_textimage':
				$this->setAttributesNjTextimage();
				break;
			default:
				$this->processedData['layout'] = 'default';
		}
	}
	
	
	protected function setAttributesNjCarousel() {
		
		if($this->data['nj_slides'] !== '0') {
			$slideRepository = \T3kk\NjBootstrap\Service\GeneralService::getInstance(SlideRepository::class);
			$this->processedData['slides'] = $slideRepository->findAllByRelation(
				'tt_content',
				'nj_slides',
				$this->data['uid']
			);
		}
	}

	protected function setAttributesNjHero() {
		if($this->data['nj_bg_image'] !== NULL) {
			$this->setImages('nj_bg_image', 1, true);
		}
	}
	
	
	protected function setAttributesNjIcons() {
		
		if($this->data['nj_icons'] !== '0') {
			$iconRepository = \T3kk\NjBootstrap\Service\GeneralService::getInstance(IconRepository::class);
			$icons = $iconRepository->findAllByRelation(
				'tt_content',
				'nj_icons',
				$this->data['uid']
			);
			
			foreach($icons as $icon) {
				if($icon->getSvgFile() !== NULL) {
					$fileReference = $icon->getSvgFile();
					$file = $fileReference->getOriginalResource()->getOriginalFile();
					
					$imageService = \T3kk\NjBootstrap\Service\GeneralService::getInstance(ImageService::class);
					
					$absoluteSource = $imageService->getImagePath($file).$file->getIdentifier();
					if(file_exists($absoluteSource)) {
						$icon->setSvgInline($imageService->getSvgInline($absoluteSource));
					}
				}
				if($icon->getSvgInline() !== '') {
					$catIdentifier = 'default';
					if($icon->getCategories() !== NULL) {
						$i = 0;
						foreach($icon->getCategories() as $category) {
							$catIdentifier = $category->getTitle();
							$catSubtitle = $category->getNjSubtitle();
							if($i > 0) { break; }
							$i++;
						}
					}
					$this->processedData['collection'][$catIdentifier]['icons'][] = $icon;
					if(!empty($catSubtitle)) {
						$this->processedData['collection'][$catIdentifier]['subtitle'] = $catSubtitle;
					}
				}
			}			
		}
	}
	
	
	protected function setAttributesNjMaps() {
		if(isset($this->data['nj_gm_api_key']) && $this->data['nj_gm_api_key'] !== '') {
			$pageRenderer = $this->getPageRenderer();
			
			if($this->data['nj_gm_marker'] !== '0') {
				$markerRepository = \T3kk\NjBootstrap\Service\GeneralService::getInstance(MarkerRepository::class);
				$this->processedData['marker'] = $markerRepository->findAllByRelation(
					'tt_content',
					'nj_gm_marker',
					$this->data['uid']
				);
			}
		}
	}
	
	
	protected function setAttributesNjTextimage() {
		
		if($this->data['image'] !== NULL) {
			$this->setImages('image', 1, true);
		}
		
		
//		if($this->processedData['image'] !== NULL) {
//			$imageService = $this->getImageService();
//			$images[] = [
//				'file' => $this->processedData['image'],
//				'id' => $this->data['uid']
//			];
//			
//			$imageService->renderResponsiveBackgroundImages(
//				$images, 
//				'.nj-textimage--image',
//				'data-uid'
//			);
//		}
	}








	protected function setAttributesValidContacts() {
		switch ($this->data['layout']) {
			case 1:
				$this->processedData['layout'] = 'compact';
				$contacts = DatabaseService::getDataByUidList($this->data['valid_contacts']);
				$this->processedData['contacts'] = $contacts;
				break;
			case 2:
				$this->processedData['layout'] = 'slider';
				$this->processedData['profiles'] = $this->fetchProfiles();
				
				$pageContacts = DatabaseService::getPagesByDoktype(125);
				if(is_array($pageContacts)) {
					$sliderBackground = ImageService::getImages(
						$table = 'pages', 
						$field = 'valid_bg_image',
						$uid = $pageContacts[0]['uid'],
						$single = true, 
						$random = false
					);
					
					if($sliderBackground !== NULL) {
						$this->processedData['sliderBackground'] = $sliderBackground;
					}
				}
				break;
			default:
				$this->processedData['layout'] = 'default';
				$contacts = DatabaseService::getDataByUidList($this->data['valid_contacts']);
				$this->processedData['contacts'] = $contacts;
		}
	}
	
	
	protected function setAttributesValidSlideshare() {
		$error = [];
		$url = $this->data['valid_url'];
		$slideshare = VariousService::getSlidesharePresentation($url);
		
		if(is_array($slideshare)) {
			$this->processedData['slideshare'] = $slideshare;
		}
	}
	
	
	protected function setAttributesValidTextImage() {
		switch ($this->data['layout']) {
			case 1:
				break;
			case 2:
				$this->processedData['layout'] = 'text-front-img-back';
				break;
			case 3:
				break;
			case 4:
				break;
			default:
				$this->processedData['layout'] = 'default';
		}
		switch ($this->data['valid_text_horizont']) {
			case 1:
				$this->processedData['text-orientation'] = 'center';
				break;
			case 2:
				$this->processedData['text-orientation'] = 'right';
				break;
			default:
				$this->processedData['text-orientation'] = 'left';
		}
		switch ($this->data['valid_text_vertical']) {
			case 1:
				$this->processedData['text-orientation'] .= ',middle';
				break;
			case 2:
				$this->processedData['text-orientation'] .= ',bottom';
				break;
			default:
				$this->processedData['text-orientation'] .= ',top';
		}
	}
	
	protected function setVariablesJobs() {
		
		$numberOfRecords = $this->data['valid_nor'];
		
		$numberOfRecordsOverall = DatabaseService::getPagesByDoktype(
			DatabaseService::DOKTYPE_JOB, 
			$GLOBALS['TSFE']->sys_language_uid, 
			$resultType = DatabaseService::DATABASE_ACTION_COUNT_ALL
		);
		
		$jobs = DatabaseService::getPagesByDoktype(
			DatabaseService::DOKTYPE_JOB, 
			$GLOBALS['TSFE']->sys_language_uid,
			DatabaseService::DATABASE_ACTION_GET_ROWS,
			'crdate DESC',
			0,
			$numberOfRecords
		);
		
		if(!empty($jobs)) {
			$i=0;
			foreach($jobs as $job) {
				$office = DatabaseService::getPageByUid($job['pid']);
				$location = DatabaseService::getLocationByUid($office['valid_location']);
				$jobs[$i]['location'] = $location;
				$i++;
			}
		}
		
		$filter = [];
		$filter['locations'] = DatabaseService::getJobLocations();
		
		
		$extSettings = VariousService::getExtSettings();
		
		$careerCategory = $extSettings['jobs.']['mainCategory'];
		
		if(is_numeric((int)$careerCategory)) {
			$categories = DatabaseService::getCategoriesByJobOffers($careerCategory);
			if($categories !== NULL && !empty($categories)) {
				$filter['categories'] = $categories;
			}
		}

		$jobsOverall = DatabaseService::getPagesByDoktype(
			DatabaseService::DOKTYPE_JOB, 
			$GLOBALS['TSFE']->sys_language_uid,
			DatabaseService::DATABASE_ACTION_GET_ROWS_ALL,
			'',
			-1,
			-1,
			[],
			$fields = 'uid'
		);
		
		
	
		$i = 0;
		$uids = '';
		foreach($jobsOverall as $job) {
			if($i !== 0) { $uids .= ','; } 
			$uids .= $job['uid'];
			$i++;
		}
		
		$this->processedData['filter'] = $filter;
		$this->processedData['jobs']['jobs'] = $jobs;
		
		$this->processedData['jobs']['limit'] = $numberOfRecords;
		$this->processedData['jobs']['overall'] = $numberOfRecordsOverall;
	}
	
	
	protected function setImages($imageField = 'image',$NOI = 1, $shuffle = true) {
		if(is_array($this->data) && $this->data['uid']) {
            $this->fileRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Resource\\FileRepository');
            $images = $this->fileRepository->findByRelation($this->table, $imageField, $this->data['uid']);
        } else{
            $images = NULL;
        }
	   
		if(is_array($images)) {
			if($shuffle) {
				shuffle($images);
			}
			
			if($NOI > 1 && count($images) > 1) {
				$proecessedImage = [];
				$limit = $NOI > count($images) ? $count($images) : $NOI;
				for($i=0;$i<=$limit;$i++) {
					$processedImages[] = $images[$i];
				}
				$this->processedData['images'] = $images;
			}
			else {
				$this->processedData['image'] = $images[0];
			}
		}
	}
	
	
	
	protected function fetchProfiles() {
		$profiles = DatabaseService::getContactsInSpotlight();
		if(is_array($profiles)) {
			
			$i = 0;
			foreach($profiles as $profile) {
				
				$backgroundImage = ImageService::getImages(
					$table = 'tt_content', 
					$field = 'valid_background',
					$uid = $profile['uid'],
					$single = true, 
					$random = false
				);
			
				if($backgroundImage !== NULL) {
					$profile['background'] = $backgroundImage;
				} 
				$profiles[$i] = $profile;
				$i++;
			}
		}
		return $profiles;
	}
	
	
	/**
     * Return an instance of ImageService using object manager
     *
     * @return ImageService
     */
    protected static function getImageService()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        return $objectManager->get(ImageService::class);
    }
	
	/**
	 * Provides a shared (singleton) instance of PageRenderer
	 *
	 * @return \TYPO3\CMS\Core\Page\PageRenderer
	 */
	protected function getPageRenderer() {
		return \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\PageRenderer::class);
	}
	
}