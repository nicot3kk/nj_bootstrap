<?php
namespace T3kk\NjBootstrap\DataProcessing;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * @author Nico Jatzkowski <nico@nj81.de>
 * @package T3kk
 * @subpackage nj_bootstrap
 */
class AbstractProcessor implements DataProcessorInterface {
	
	public function process(ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration, array $processedData) {
		
	}

	/**
	 * @param array $array
	 * @param boolean $single
	 * @param boolean $random
	 */
	protected function randomize($array,$single,$random) {
		if($single) {
			if($random) {
				shuffle($array);
			}
				return $array[0];
			} else {
			return $array;
		}
	}
}