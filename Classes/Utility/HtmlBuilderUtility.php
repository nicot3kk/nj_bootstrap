<?php
namespace T3kk\NjBootstrap\Utility;

/**
 * @author Nico Jatzkowski <nico@t3kk.de>
 * @package T3kk
 * @subpackage nj_bootstrap
 */
class HtmlBuilderUtility
{
	const _ATTR_ALIGN					= 'align';
	const _ATTR_ALT						= 'alt';
	const _ATTR_CLASS					= 'class';
	const _ATTR_DATA_ANIMATE_CLICK		= 'data-animate-click';
	const _ATTR_DATA_ANIMATE_HOVER		= 'data-animate-hover';
	const _ATTR_DATA_DEVICE				= 'data-device';
	const _ATTR_DATA_ROLE				= 'data-role';
	const _ATTR_DATA_ORIENTATION		= 'data-orientation';
	const _ATTR_HREF					= 'href';
	const _ATTR_TARGET					= 'target';
	const _ATTR_TITLE					= 'title';
	const _ATTR_TAG						= 'tag';
	
	const _ATTR_ID						= 'id';
	const _ATTR_ROLE					= 'role';
	
	const _ALIGN_CENTER					= 'center';
	const _ALIGN_LEFT					= 'left';
	const _ALIGN_RIGHT					= 'right';
	
	const _CLASS_PREFIX						= 'nj';
	const _CLASS_BASE						= 'nj base';
	const _CLASS_CONTAINER					= 'nj container';
	const _CLASS_SECTION					= 'nj section';
	const _CLASS_WRAPPER					= 'nj wrapper';
	
	const _ELEMENT_SECTION_ADDRESS			= 'address';
	const _ELEMENT_SECTION_ARTICLE			= 'article';
	const _ELEMENT_SECTION_ASIDE			= 'aside';
	const _ELEMENT_SECTION_BODY				= 'body';
	const _ELEMENT_SECTION_FOOTER			= 'footer';
	const _ELEMENT_SECTION_HEADER			= 'header';
	const _ELEMENT_SECTION_HEADLINE_1		= 'h1';
	const _ELEMENT_SECTION_HEADLINE_2		= 'h2';
	const _ELEMENT_SECTION_HEADLINE_3		= 'h3';
	const _ELEMENT_SECTION_HEADLINE_4		= 'h4';
	const _ELEMENT_SECTION_HEADLINE_5		= 'h5';
	const _ELEMENT_SECTION_HEADLINE_6		= 'h6';
	const _ELEMENT_SECTION_MAIN				= 'main';
	const _ELEMENT_SECTION_NAV				= 'nav';
	const _ELEMENT_SECTION_SECTION			= 'section';
	
	const _ELEMENT_STD_BUTTON				= 'button';
	const _ELEMENT_STD_DIV					= 'div';
	
	const _EMPTY_SPACE						= ' ';
	
	const _TAG_ACTION_OPEN					= 'open';
	const _TAG_ACTION_CLOSE					= 'close';
	const _TAG_ACTION_COMPLETE				= 'complete';
	
	const _VAL_TARGET_BLANK = 'blank';
	const _VAL_TARGET_SELF = 'self';
	
	public static function attribute($name,$value)
	{
		return $name.'="'.$value.'"';
	}
	
	private static function tagOpen($element)
	{
		return '<'.$element;
	}
	
	private static function tagClose()
	{
		return '>';
	}
	
	public static function elementOpen($element, $tagAction)
	{
		switch($tagAction)
		{
			case self::_TAG_ACTION_OPEN:
				return self::tagOpen($element);
			case self::_TAG_ACTION_CLOSE:
				return self::tagClose();
			case self::_TAG_ACTION_COMPLETE:
				return self::tagOpen($element).self::tagClose();
		}
	}
	
	public static function elementClose($element)
	{
		return '</'.$element.'>';
	}
	
	/**
	 * @param string $classes
	 * @param string $additionalClasses
	 * @return string
	 */
	public static function concatClasses($classes = '',$additionalClasses = '')
	{
		/** @var $concatenation string */ 
		$concatenation = $classes;
		
		$additionalClassesArray = explode(' ', $additionalClasses);
		
		foreach($additionalClassesArray as $additionalClass)
		{
			if(strpos($concatenation,$additionalClass)===false){
				$concatenation .= ' '.$additionalClass;
			}
		}
		return $concatenation;
	}
	
	/**
	 * Values set in TCEFORM.ts:
	 * 0 = 1
	 * 1 = 2
	 * 2 = 3
	 * 3 = 4
	 * 4 = 6
	 * Grid of 12 cols -> 12 / $value = col-x
	 * 
	 * @param int $value
	 * @param boolean $returnCssClass
	 * @param string $classname
	 * 
	 */
	public static function getCssGridCol($value = 0, $returnCssClass = true, $classname = 'col') {
		
		$cols = [
			0 => 1,
			1 => 2,
			2 => 3,
			3 => 4,
			4 => 6
		];
		$tmp = (12 / $cols[$value]);
		return $returnCssClass ? $classname.'-'.$tmp : $cols[$value];
	} 
}