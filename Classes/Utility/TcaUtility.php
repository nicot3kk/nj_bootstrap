<?php
namespace T3kk\NjBootstrap\Utility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use T3kk\NjBootstrap\Service\GeneralService;
use T3kk\NjBootstrap\Utility\FluidTemplateUtility;

class TcaUtility {
	
	/**
	 * @var int 
	 */
	var $contentUid;
	
	/**
	 * @var \T3kk\NjBootstrap\Utility\FluidTemplateUtility
	 */
	var $fluidTemplate = NULL;
	
	/**
	 * @var \TYPO3\CMS\Core\Page\PageRenderer
	 */
	var $pageRenderer = NULL;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
	 * @inject
	 */
	public $configurationManager;
	
	/**
	 * @param array $PA
	 * @param string $templateName
	 */
	private function init($PA,$templateName) {
		//$this->setContentUid($PA);
		//$this->setPageRenderer();
		$this->setFluidTemplate(ucfirst($templateName));
	}
	
	/**
	 * @param array $PA
	 * @param \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer $cObj
	 * @return string
	 */
	public function animationSelection($PA,$cObj) {
		$assignValues = [
			'pa' => $PA,
			'cObj' => $cObj,
		];
		
		if(isset($PA['row'])) {
			$imagesUidList = $PA['row']['image'];
			$images = explode(",", $imagesUidList);
			if(!empty($images)) {
				shuffle($images);
				$assignValues['image'] = $images[0];
			}
		}
		$fluidTemplate = $this->getFluidTemplate(__FUNCTION__);
		if($fluidTemplate !== NULL) {
			$fluidTemplate->assignMultiple($assignValues);
			return $fluidTemplate->render();
		} else {
			return 'error';
		}
	}
	
	public function loadAssets($PA,$cObj) {
		$this->pageRenderer = GeneralService::getInstance(PageRenderer::class);
		$this->pageRenderer->addJsFile('../typo3conf/ext/nj_bootstrap/Resources/Public/Js/backend.js');
		$this->pageRenderer->addCssFile('../typo3conf/ext/nj_bootstrap/Resources/Public/Css/tca.css');
	
		switch($PA['row']['CType'][0]) {
			case 'nj_textimage':
				$this->pageRenderer->addCssFile('../typo3conf/ext/nj_bootstrap/Resources/Public/Vendor/Css/Animate.css/3.5.1/animate.min.css');
			case 'nj_maps':
				if(isset($PA['row']['nj_gm_api_key']) && $PA['row']['nj_gm_api_key'] !== '') {
					$this->pageRenderer->addJsFile('http://maps.googleapis.com/maps/api/js?sensor=false&key='.$PA['row']['nj_gm_api_key']);
				}
		}
		
	}
	
	
	public function geoLocation($PA,$cObj) {
		$assignValues = [
			'row' => $PA['row']
		];
		$fluidTemplate = $this->getFluidTemplate(__FUNCTION__);
		if($fluidTemplate !== NULL) {
			$fluidTemplate->assignMultiple($assignValues);
			return $fluidTemplate->render();
		} else {
			return 'error';
		}
	}
	
	
	public function infoText($PA, $fObj) {

		return 
			'<div class="callout callout-info">'
			. \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($PA['parameters']['text'], 'nj_bootstrap')
			.'</div>';
			
    } //end of function infoText
	
	
	/**
	 * @param array $PA
	 */
	private function setContentUid($PA) {
		$this->contentUid = $PA['row']['uid'];
	}
	
	/**
	 * @param string $templateName
	 */
	private function setFluidTemplate($templateName) {
		if($templateName !== NULL) {
			$this->fluidTemplate = GeneralService::getInstance(FluidTemplateUtility::class);
			$this->fluidTemplate->setTemplatePathAndFilename(
				GeneralUtility::getFileAbsFileName(
					FluidTemplateUtility::_DEFAULT_DIR_TEMPLATES_BE . $templateName.'.html'
				)
			);
		}
	}
	
	/**
	 * @param string $templateName
	 */
	private function getFluidTemplate($templateName) {
		if($templateName !== NULL) {
			$fluidTemplate = GeneralService::getInstance(FluidTemplateUtility::class);
			$fluidTemplate->setTemplatePathAndFilename(
				GeneralUtility::getFileAbsFileName(
					FluidTemplateUtility::_DEFAULT_DIR_TEMPLATES_BE . $templateName.'.html'
				)
			);
			return $fluidTemplate;
		}
		return NULL;
	}
	
	private function setPageRenderer() {
		
	}
	
	
	/**
	 * @param string $table
	 * @param array $columns
	 */
	public static function addTCAcolumns($table,$columns) {
		if(!empty($table) && !empty($columns)) {
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
				$table,
				$columns
			);
		}
	}
	
	/**
	 * @param string $table
	 * @param string $fields
	 * @param string $position
	 */
	public static function addTCAfields($table,$fields,$position = '') {
		if(!empty($table) && !empty(fields)) {
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
				$table,
				$fields,
				'',
				$position
			);
		}
	}	
}