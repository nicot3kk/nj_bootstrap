<?php
namespace T3kk\NjBootstrap\Domain\Repository;


class IconRepository extends \T3kk\NjBootstrap\Domain\Repository\AbstractRepository {
	
	/**
	 * Initializes the repository.
	 * @return void
	 * @see \TYPO3\CMS\Extbase\Persistence\Repository::initializeObject()
	 */
	public function initializeObject() 
	{}
	
	protected $defaultOrderings = array(
		'foreign_sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
	);
	
}