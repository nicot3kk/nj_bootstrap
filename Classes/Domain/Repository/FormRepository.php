<?php
namespace T3kk\NjBootstrap\Domain\Repository;
use T3kk\NjBootstrap\Service\GeneralService;

class FormRepository extends \T3kk\NjBootstrap\Domain\Repository\AbstractRepository {
	
	/**
	 * Initializes the repository.
	 * @return void
	 * @see \TYPO3\CMS\Extbase\Persistence\Repository::initializeObject()
	 */
	public function initializeObject() 
	{
		$repository = GeneralService::getClassName(__CLASS__);
		parent::init($repository);
	}
	
	protected $defaultOrderings = array(
		'crdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
	);
	
}