<?php
namespace T3kk\NjBootstrap\Domain\Repository;
use T3kk\NjBootstrap\Service\GeneralService;

class AbstractRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {
	
	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
	 * @inject
	 */
	protected $configurationManager;
	
	
	protected $defaultOrderings = array(
            'crdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
	);
	
	
	/**
	 * 
	 * @param string $repository
	 */
	protected function init($repository) {	
		
		$settings = $this->configurationManager->getConfiguration(
			\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT,
			'tx_njbootstrap'
		);
		
		$storagePid = (int)$settings['plugin.']['tx_njbootstrap.']['persistence.']['storagePid'];
		
		$pids = $settings['plugin.']['tx_njbootstrap.']['persistence.']['model.'];
		foreach($pids as $key => $value) {
			
			if($key === $repository) {
				if((int)$value > 0) {
					$storagePid = $value;
				}
			}
		}
		
		if(!is_null($storagePid)) {
			
			$querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
			$querySettings->setRespectStoragePage(TRUE);
			$querySettings->setStoragePageIds([$storagePid]);
			$this->setDefaultQuerySettings($querySettings);	
		}
	}
	
	
	/**
	 * @param array $storagePageIds
	 * @param boolean
	 * @return  void
	 */
	protected function setQuerySettings(array $storagePageIds, $respectSysLanguage = TRUE) {
		
		//$querySettings = GeneralService::getInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
		
		//$this->defaultQuerySettings->
		//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($querySettings, __CLASS__ . '::' . __FUNCTION__);
		//$querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
		/**
		if(!empty($storagePageIds))
		{
			$querySettings->setStoragePageIds($storagePageIds);
			$querySettings->setRespectStoragePage(TRUE);
		}
		else 
		{
			$querySettings->setRespectStoragePage(FALSE);
		}
		$querySettings->setRespectSysLanguage(TRUE);
		$this->setDefaultQuerySettings($querySettings);
		 * 
		 */
	}
	
	/**
	 * @param string $table
	 * @param string $field
	 * @param int $uid
	 */
	public function	findAllByRelation($table,$field,$uid) {
		
		$this->defaultQuerySettings = GeneralService::getInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
		$this->defaultQuerySettings->setRespectStoragePage(FALSE);
		$this->defaultQuerySettings->setRespectSysLanguage(TRUE);
		
		$query = $this->createQuery();
		$query->matching($query->equals('foreign_table', $table));
		$query->matching($query->equals('foreign_field', $field));
		$query->matching($query->equals('foreign_uid', $uid));

		return $query->execute();
	}
	
}
