<?php
namespace T3kk\NjBootstrap\Domain\Model;

class Slide extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	
	/**
	 * @var string
	 */
	protected $content;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $image;
	
	/**
	 * @var int 
	 */
	protected $slideType;
	
	/**
	 * @var string
	 * @validate StringLength(maximum = 128)
	 */
	protected $subtitle;
	
	/**
	 * @var string
	 * @validate StringLength(minimum = 3, maximum = 64)
	 */
	protected $title;
	
	/* ***************************************************** */

	/**
	 * Constructs a new slide
	 * @return AbstractObject
	 */
	public function __construct() {}

	/* ***************************************************** */
	
	/**
     * @param string
     * @return void
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
	
	
	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
	 * @return void
	 */
	public function setImage($image) 
	{
		$this->image = $image;
	}
	
	
	/**
     * @param int
     * @return void
     */
    public function setSlideType($slideType)
    {
        $this->slideType = $slideType;
    }

    /**
     * @return string
     */
    public function getSlideType()
    {
        return $this->slideType;
    }
	
	
	/**
     * @param string
     * @return void
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }
	
	
	/**
     * @param string
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
