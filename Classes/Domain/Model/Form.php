<?php
namespace T3kk\NjBootstrap\Domain\Model;

class Form extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	
	/**
	 * @var int
	 */
	protected $crdate;
	
	/**
	 * @var string
	 */
	protected $country;
	
	/**
	 * @var string
	 */
	protected $email;
	
	/**
	 * @var string
	 */
	protected $firm;
	
	/**
	 * @var string
	 */
	protected $firstName;
	
	/**
	 * @var string
	 */
	protected $lastName;

	/**
	 * @var string
	 */
	protected $location;

	/**
	 * @var string 
	 */
	protected $logIp;
	
	/**
	 * @var string
	 */
	protected $message;
	
	/**
	 * @var string
	 */
	protected $mobile;
	
	/**
	 * @var string
	 */
	protected $name;
	
	/**
	 * @var string
	 */
	protected $phone;
	
	/**
	 * @var string
	 */
	protected $street;
	
	/**
	 * @var string
	 */
	protected $subject;
	
	/**
	 * @var int
	 */
	protected $zipCode;
	
	/**
	 * @var string
	 */
	protected $url;
	
	
	/* ***************************************************** */

	/**
	 * Constructs a new slide
	 * @return AbstractObject
	 */
	public function __construct() {}

	/* ***************************************************** */
	
	/**
     * @param int $crdate
     * @return void
     */
    public function setCrdate($crdate) {
        $this->crdate = $crdate;
    }

    /**
     * @return int
     */
    public function getCrdate() {
        return $this->crdate;
    }
	
	
	/**
     * @param string $country
     * @return void
     */
    public function setCountry($country) {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCountry() {
        return $this->country;
    }
	
	
	/**
     * @param string $email
     * @return void
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }
	
	
	/**
     * @param string $firm
     * @return void
     */
    public function setFirm($firm) {
        $this->firm = $firm;
    }

    /**
     * @return string
     */
    public function getFirm() {
        return $this->firm;
    }
	
	
	/**
     * @param string $firstName
     * @return void
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }
	
	
	/**
     * @param string $lastName
     * @return void
     */
    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getLastName() {
        return $this->lastName;
    }
	
	
	/**
     * @param string $location
     * @return void
     */
    public function setLocation($location) {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }
	
	
	/**
     * @param string $logIp
     * @return void
     */
    public function setLogIp($logIp) {
        $this->logIp = $logIp;
    }

    /**
     * @return string
     */
    public function getLogIp() {
        return $this->logIp;
    }
	
	
	/**
     * @param string $message
     * @return void
     */
    public function setMessage($message) {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }
	
	
	/**
     * @param string $mobile
     * @return void
     */
    public function setMobile($mobile) {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getMobile() {
        return $this->mobile;
    }
	
	
	/**
     * @param string $name
     * @return void
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }
	
	
	/**
     * @param string $phone
     * @return void
     */
    public function setPhone($phone) {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }
	
	
	/**
     * @param string $street
     * @return void
     */
    public function setStreet($street) {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getStreet() {
        return $this->street;
    }
	
	
	/**
     * @param string $subject
     * @return void
     */
    public function setSubject($subject) {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getSubject() {
        return $this->subject;
    }
	
	
	/**
     * @param int $zipCode
     * @return void
     */
    public function setZipCode($zipCode) {
        $this->zipCode = $zipCode;
    }

    /**
     * @return int
     */
    public function getZipCode() {
        return $this->zipCode;
    }
	
	
	/**
     * @param string $url
     * @return void
     */
    public function setUrl($url) {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl() {
        return $this->url;
    }
	
}