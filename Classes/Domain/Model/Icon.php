<?php
namespace T3kk\NjBootstrap\Domain\Model;

class Icon extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	
	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3kk\NjBootstrap\Domain\Model\Category>
	 */
	protected $categories;
	
	/**
	 * @var string
	 */
	protected $content;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 */
	protected $svgFile;
	
	/**
	 * @var string
	 */
	protected $svgInline;
	
	
	/**
	 * @var string
	 * @validate StringLength(minimum = 3, maximum = 64)
	 */
	protected $title;
	
	/* ***************************************************** */

	/**
	 * Constructs a new slide
	 * @return AbstractObject
	 */
	public function __construct() {}

	/* ***************************************************** */
	
	
	/**
	 * @param \TYPO3\CMS\Extbase\Domain\Model\Category $category
	 * @return void
	 */
	public function addCategory(\TYPO3\CMS\Extbase\Domain\Model\Category $category) {
		$this->categories->attach($category);
	}
	
	/**
	 * @param \TYPO3\CMS\Extbase\Domain\Model\Category $categoryToRemove 
	 * @return void
	 */
	public function removeCategory(\TYPO3\CMS\Extbase\Domain\Model\Category $categoryToRemove) {
		$this->categories->detach($categoryToRemove);
	}
	
	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category> $categories
	 */
	public function getCategories() {
		return $this->categories;
	}
	
	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\Category> $categories
	 * @return void
	 */
	public function setCategories(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $categories) {
		$this->categories = $categories;
	}
	
	
	/**
     * @param string
     * @return void
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
	
	
	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $image
	 */
	public function getSvgFile()
	{
		return $this->svgFile;
	}

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $svgFile
	 * @return void
	 */
	public function setSvgFile($svgFile) 
	{
		$this->svgFile = $svgFile;
	}
	
	
	/**
     * @param string $svgInline
     * @return void
     */
    public function setSvgInline($svgInline)
    {
        $this->svgInline = $svgInline;
    }

    /**
     * @return string
     */
    public function getSvgInline()
    {
        return $this->svgInline;
    }
	
	
	/**
     * @param string
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}