<?php
namespace T3kk\NjBootstrap\Domain\Model;


class Category extends \TYPO3\CMS\Extbase\Domain\Model\Category {
	
	/**
	 * @var \T3kk\NjBootstrap\Domain\Repository\CategoryRepository
	 * @inject
	 */
	protected $categoryRepository;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3kk\NjBootstrap\Domain\Model\Category>
	 */
	protected $children;
	
	/**
	 * @var int 
	 */
	protected $level;
	
	/**
	 * @var string 
	 */
	protected $njSubtitle;
	
	
	/* ***************************************************** */

	/**
	 * Constructs a new slide
	 * @return AbstractObject
	 */
	public function __construct() {}

	/* ***************************************************** */
	
	
	/**
	 * @return void
	 */
	public function isChild() {
		if($this->getParent() !== NULL) {
			return true;
		} else {
			return false;
		}
	}
	
	
	/**
	 * @return boolean
	 */
	public function hasChildren() {
		if(count($this->getChildren()) > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @param int $level
	 */
	public function setLevel($level) {
		$this->level = $level;
	}
	
	/**
	 * @return int
	 */
	public function getLevel() {
		return $this->level;
	}
	
	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3kk\NjBootstrap\Domain\Model\Category> $children
	 */
	public function setChildren($children) {
		$this->children = $children;
	}
	
	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3kk\NjBootstrap\Domain\Model\Category>
	 */
	public function getChildren() {
		if($this->children === NULL) {
			$this->children = $this->productRepository->findByParent($this);
		}
		return $this->children;
	}
	
	
	/**
	 * @param string $njSubtitle
	 */
	public function setNjSubtitle($njSubtitle) {
		$this->njSubtitle = $njSubtitle;
	}
	
	/**
	 * @return string
	 */
	public function getNjSubtitle() {
		return $this->njSubtitle;
	}
	
	
} //end of class