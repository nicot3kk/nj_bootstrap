<?php
namespace T3kk\NjBootstrap\ViewHelpers;
use T3kk\NjBootstrap\Service\ImageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * DESCRIPTION: 
 * 
 * @author Nico Jatzkowski <nico@nj81.de>
 * @package T3kk
 * @subpackage nj_bootstrap
 */
class AbstractViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {
	
	const ARGUMENT_CSS_SELECTOR = 'cssSelector';
	const ARGUMENT_IMAGE_SRC = 'src';
	
	/**
	 * @var \TYPO3\CMS\Core\Resource\FileRepository
	 * @inject
	 */
	protected $fileRepository;
	
	
	/**
	 * @param string $argument Name of the array key in arguments
	 */
	protected function argumentGet($argument)
	{
		return $this->arguments[$argument];
	}
	
	/**
	 * @param string $argument Name of the array key in arguments
	 * @return boolean
	 */
	protected function argumentIsNotEmpty($argument)
	{
		if($this->argumentIsSet($argument))
		{
			if($this->argumentGet($argument) !== '')
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param string $argument Name of the array key in arguments
	 * @return boolean
	 */
	protected function argumentIsSet($argument)
	{
		if($this->argumentGet($argument) !== NULL)
		{
			return true;
		}
		return false; 
	}
	
	/**
	 * @param string $argument Name of the array key in arguments
	 * @return boolean
	 */
	protected function argumentIsTrue($argument) {
		if(is_bool($this->argumentGet($argument))) {
			if($this->argumentIsSet($argument)) {
				if($this->argumentGet($argument) === TRUE) {
					return true;
				}
			}
		}
		else {
			if($this->argumentIsSet($argument)) {
				if($this->argumentGet($argument) > 0) {
					return true;
				}
			}
		}
		return false;
	}	
	
	/**
     * Return an instance of ImageService using object manager
     *
     * @return ImageService
     */
    protected static function getImageService()
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        return $objectManager->get(ImageService::class);
    }
	
}
