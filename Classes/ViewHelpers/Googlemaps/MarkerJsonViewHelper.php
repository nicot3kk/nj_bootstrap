<?php
namespace T3kk\NjBootstrap\ViewHelpers\Googlemaps;
use T3kk\NjBootstrap\Domain\Model\Marker;

class MarkerJsonViewHelper extends \T3kk\NjBootstrap\ViewHelpers\AbstractViewHelper {
	
	const ARGUMENT_MARKER = 'marker';
	
	/**
     * Initialize arguments
     */
    public function initializeArguments() {
        parent::initializeArguments();
        $this->registerArgument(self::ARGUMENT_MARKER, 'TYPO3\CMS\Extbase\Persistence\Generic\QueryResult', 'All marker of the map.');
	}
	
	public function render() {
		$mapMarker = [];
		if($this->argumentIsNotEmpty(self::ARGUMENT_MARKER)) {
			$i=0;
			foreach($this->argumentGet(self::ARGUMENT_MARKER) as $marker) {
				if(is_a($marker, Marker::class)) {
					$mapMarker[$i]['city'] = $marker->getCity();
					$mapMarker[$i]['country'] = $marker->getCountry();
					$mapMarker[$i]['latitude'] = $marker->getLatitude();
					$mapMarker[$i]['longitude'] = $marker->getLongitude();
					$mapMarker[$i]['street'] = $marker->getStreet();
					$mapMarker[$i]['title'] = $marker->getTitle();
					$mapMarker[$i]['zipCode'] = $marker->getZipCode();
				}
				$i++;
			}
		}
		if(!empty($mapMarker)) {
			return json_encode($mapMarker);
		}
	}
}