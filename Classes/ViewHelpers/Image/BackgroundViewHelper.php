<?php
namespace T3kk\NjBootstrap\ViewHelpers\Image;
use T3kk\NjBootstrap\Service\ImageService;
use TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Fluid\Core\ViewHelper\Exception;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;


class BackgroundViewHelper extends \T3kk\NjBootstrap\ViewHelpers\AbstractViewHelper {
	
	const ARGUMENT_CSS_SELECTOR = 'css-selector';
	const ARGUMENT_IMAGE = 'image';
	const ARGUMENT_UID = 'uid';
	
	/**
     * Initialize arguments
     */
    public function initializeArguments() {
        parent::initializeArguments();
		$this->registerArgument(self::ARGUMENT_CSS_SELECTOR, 'string', 'The Image.',true,NULL);
        $this->registerArgument(self::ARGUMENT_IMAGE, '\TYPO3\CMS\Core\Resource\FileReference', 'The Image.',true,NULL);
    }
	
	/**
	 * @see https://docs.typo3.org/typo3cms/TyposcriptReference/ContentObjects/ImgResource/
     * @throws Exception
     * @return string path to the image
	 */
	public function render() {
		$images = [
			0 => $this->argumentGet(self::ARGUMENT_IMAGE)
		];
		
		$imageService = $this->getImageService();
		$imageService->renderResponsiveBackgroundImages(
			$images, 
			$this->argumentGet(self::ARGUMENT_CSS_SELECTOR)
		);
    }
}
