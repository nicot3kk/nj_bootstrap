<?php
namespace T3kk\NjBootstrap\ViewHelpers\Image;
use T3kk\NjBootstrap\Service\ImageService;

class SvgViewHelper extends \T3kk\NjBootstrap\ViewHelpers\AbstractViewHelper {
	
	const ARGUMENT_SOURCE = 'src';
	const ARGUMENT_INLINE = 'inline';
	/**
     * Initialize arguments
     */
    public function initializeArguments() {
        parent::initializeArguments();
		$this->registerArgument(self::ARGUMENT_INLINE, 'boolean', 'Option if svg should be rendered as inline element',false,true);
        $this->registerArgument(self::ARGUMENT_SOURCE, 'string', 'The source of the svg-file',true,NULL);
    }
	
	/**
	 * @see https://docs.typo3.org/typo3cms/TyposcriptReference/ContentObjects/ImgResource/
     * @throws Exception
     * @return string path to the image
	 */
	public function render() {
		if($this->argumentIsNotEmpty(self::ARGUMENT_SOURCE)) {
			if($this->argumentIsTrue(self::ARGUMENT_INLINE)) {
				$imageService = \T3kk\NjBootstrap\Service\GeneralService::getInstance(ImageService::class);
				$image = $imageService->getImage($this->argumentGet(self::ARGUMENT_SOURCE), NULL, FALSE);
				$absoluteSource = $_SERVER['DOCUMENT_ROOT'].$imageService->getImagePath($image).$image->getIdentifier();
				if(\file_exists($absoluteSource)) {
					$inlineSvg = $imageService->getSvgInline($absoluteSource);
					return $inlineSvg;
				}
			}
		}					
    }
}
