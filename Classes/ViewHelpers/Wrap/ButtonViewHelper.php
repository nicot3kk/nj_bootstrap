<?php
namespace T3kk\NjBootstrap\ViewHelpers\Wrap;

/**
 * @author Nico Jatzkowski <nico@t3kk.de>
 * @package T3kk
 * @subpackage nj_bootstrap
 */
class ButtonViewHelper extends \T3kk\NjBootstrap\ViewHelpers\Wrap\AbstractViewHelper {
	
	const _ARG_DISABLED = 'disabled';
	
	protected $dataAttributes = [];
	
	public function __construct() {
		parent::__construct();
	}
	
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument(self::_ARG_IS_AJAX, 'boolean', 'Button triggers an ajax action.',FALSE,FALSE);
		$this->registerArgument(self::_ARG_ACTION, 'string', 'Button triggers an ajax action.',FALSE,NULL);
		$this->registerArgument(self::_ARG_DISABLED, 'boolean', 'Status of the button.',FALSE,FALSE);
	}
	
	public function initialize() {
		parent::initialize();
		$this->extSettings = $this->getExtSettings();	
		if($this->argumentIsSet(self::ARGUMENT_CONTENT_DATA)) {
			$this->data = $this->argumentGet(self::ARGUMENT_CONTENT_DATA);
		} 
	}
	
	protected function setContent() {
		if($this->argumentIsNotEmpty(self::ARGUMENT_LABEL)) {
			return $this->argumentGet(self::ARGUMENT_LABEL);
		} else {
			if(!empty($this->data)) {
				return $this->data['nj_btn_label'];
			} else {
				return $this->renderChildren();
			}
		}
	}
	
	public function renderChildren() {
		return parent::renderChildren();
	}
	
	public function render() {
		$this->tag->setContent($this->setContent());
		$this->setDataAttributes();
		if(!empty($this->tag->getContent())) {
			$this->tag->setTagName('button');
			$this->setButtonStatus();
			$this->setTagClass();
			return $this->tag->render();
		} 
		return '';
	}
	
	protected function setButtonStatus() {
		if($this->argumentIsTrue(self::_ARG_DISABLED)) {
			$this->tag->addAttribute('disabled',1);
			$this->tag->addAttribute('data-status', 1);
		}
		else {
			$this->tag->addAttribute('disabled',0);
			$this->tag->addAttribute('data-status', 0);
		}
	}
	
	protected function setDataAttributes() {
		$this->setJsDataAttributes();
		$this->setAdditionalDataAttributes($this->dataAttributes);
	}
	
	protected function setJsDataAttributes() {
		if(!empty($this->data) 
			&& (isset($this->data['nj_btn_type']) && $this->data['nj_btn_type'] == 2)
			&& !empty($this->data['nj_btn_action'])
			&& !empty($this->data['nj_btn_action_data'])) {
			
			$this->dataAttributes['action'] = $this->data['nj_btn_action'];
			if($this->data['nj_btn_is_ajax'] == 1) {
				$this->dataAttributes['is-ajax'] = $this->data['nj_btn_is_ajax'];
			}
			
			$exlodeData = explode(';',$this->data['nj_btn_action_data']);
			if(!empty($exlodeData)) {
				foreach($exlodeData as $data) {
					$dataAttribute = explode(':',$data);
					$this->dataAttributes[$dataAttribute[0]] = $dataAttribute[1];
				}
			}	
		}
		
		if($this->argumentIsTrue(self::_ARG_IS_AJAX)) {
			$this->dataAttributes['is-ajax'] = 1;
		}
		if($this->argumentIsSet(self::_ARG_ACTION)) {
			$this->dataAttributes['action'] = $this->argumentGet(self::_ARG_ACTION);
		}
	}
	
	
	protected function setTagClass() {
		//\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($this->data, __CLASS__ . '::' . __FUNCTION__);
		$classes = self::_VALUE_CLASS_BUTTON;
		
		if(!empty($this->data) && is_string($this->data['nj_btn_class'])) {
			$classes .= ' '.self::_VALUE_CLASS_BUTTON.'--'.$this->data['nj_btn_class'];
		}
		
		if($this->argumentIsSet('class')) {
			$classes .= ' '.self::_VALUE_CLASS_BUTTON.'--'.$this->argumentGet('class');
		} else {
			if(!empty($this->data) && is_string($this->data['nj_btn_class'])) {
				$classes .= ' '.self::_VALUE_CLASS_BUTTON.'--'.$this->data['nj_btn_class'];
			}
		}
		
		$this->tag->addAttribute('class', $classes);
	}
	
	
	
}
