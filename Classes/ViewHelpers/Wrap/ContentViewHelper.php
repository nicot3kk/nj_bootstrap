<?php
namespace T3kk\NjBootstrap\ViewHelpers\Wrap;

/**
 * @author Nico Jatzkowski <nico@t3kk.de>
 * @package T3kk
 * @subpackage nj_bootstrap
 */
class ContentViewHelper extends \T3kk\NjBootstrap\ViewHelpers\Wrap\AbstractViewHelper
{	
	public function __construct() {
		parent::__construct();
	}
	
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument(self::ARGUMENT_CONTENT_TYPE, 'string', 'Content type of the element.',FALSE,NULL);
		$this->registerArgument(self::_ARG_DISPLAY_TYPE, 'string', 'Display type of the element.',FALSE,self::_VALUE_DEFAULT);
	}
	
	public function initialize() {
		parent::initialize();
		$this->extSettings = parent::getExtSettings();	
		if($this->argumentIsSet(self::ARGUMENT_CONTENT_DATA)) {
			$this->data = $this->argumentGet(self::ARGUMENT_CONTENT_DATA);
		} 
	}
	
	public function renderChildren() {
		if($this->extSettings['wrap.']['content.']['inner'] === '1') {
			return '<div class="nj-wrapper">'.parent::renderChildren().'</div>';
		}
		return parent::renderChildren();
	}


	public function render() {
		$this->tag->setContent($this->renderChildren());
		$this->setTagName();
		$this->setTagId();
		$this->setTagClass();
		$this->setTagAttributes();
		return $this->tag->render();
	}
	
	
	protected function setTagAttributes() {
		if(!empty($this->data)) {
			$this->tag->addAttribute('data-uid',$this->data['uid']);
		}
		$this->setTagAttributeContentType();
		$this->setTagAttributeDisplayType();
		$this->setTagAttributeListType();
		$this->setTagAttributeColorscheme();
	}
	
	/**
	 * @override
	 */
	protected function setTagName() {
		if($this->argumentIsNotEmpty(self::ARGUMENT_RENDER_TYPE)) {
			$this->tag->setTagName($this->argumentGet(self::ARGUMENT_RENDER_TYPE));
		} else {
			$this->tag->setTagName($this->extSettings['wrap.']['content.']['renderType']);
		}
	}
	
	protected function setTagClass() {
		$classes = $this->extSettings['wrap.']['content.']['className'];
		
		if($this->extSettings['wrap.']['content.']['base'] === '1') {
			$classes .= ' nj-base';
		}
		
		if(!empty($this->data) && !empty($this->data['nj_alignment'])) {
			$classes .= ' ce-align ce-align--'.$this->data['nj_alignment'];
		}
		
		$this->tag->addAttribute('class', $classes);
	}
	
	protected function setTagId() {
		if(!empty($this->data)) {
			if((int)$this->data['colPos'] === 10 || $this->argumentGet(self::_ARG_DISPLAY_TYPE) === self::_VALUE_RENDER_VERSION_FLASH_MESSAGE) {
				$this->tag->addAttribute('id','fm'.$this->data['uid']);
			} else {
				$this->tag->addAttribute('id','ce'.$this->data['uid']);
			}
		}
	}
	
	protected function setTagAttributeColorscheme() {
		if(isset($this->data['nj_color_sheme']) && $this->data['nj_color_sheme'] !== '' && $this->data['nj_color_sheme'] !== NULL) {
			$this->tag->addAttribute('data-color-sheme',$this->data['nj_color_sheme']);
		}
	}
	
	protected function setTagAttributeContentType() {
		if(isset($this->data['CType']) && !$this->argumentIsset(self::ARGUMENT_CONTENT_TYPE)) {
			$this->tag->addAttribute('data-ctype',$this->data['CType']);
		} else {
			if($this->argumentIsset(self::ARGUMENT_CONTENT_TYPE)) {
				$this->tag->addAttribute('data-ctype',$this->argumentGet(self::ARGUMENT_CONTENT_TYPE));
			}
			else {
				throw new \Exception('something went wrong');
			}
		}
		if(isset($this->data['CType']) && $this->data['CType'] === 'list') {
			
		}
	}
	
	protected function setTagAttributeListType() {
		if(isset($this->data['CType']) && $this->data['CType'] === 'list') {
			$this->tag->addAttribute('data-ltype',$this->data['list_type']);
		}
	}
	
	protected function setTagAttributeDisplayType() {
		switch((int)$this->data['colPos']) {
			case 10:
				$this->tag->addAttribute('data-dtpye','flashMessage');
				break;
		}
	}
}