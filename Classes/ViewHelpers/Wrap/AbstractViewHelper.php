<?php
namespace T3kk\NjBootstrap\ViewHelpers\Wrap;

/**
 * @author Nico Jatzkowski <nico@t3kk.de>
 * @package T3kk
 * @subpackage nj_bootstrap
 */
class AbstractViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper
{	
	const ARGUMENT_CONTENT_TYPE = 'contentType';
	const ARGUMENT_CONTENT_DATA = 'contentData';
	const ARGUMENT_RENDER_TYPE = 'renderType';
	
	const _ARG_ACTION = 'action';
	const _ARG_DISPLAY_TYPE = 'displayType';
	const _ARG_IS_AJAX = 'isAjax';
	const _ARG_TYPE = 'type';
	
	const ARGUMENT_LABEL = 'label';
	const ARGUMENT_IS_AJAX = 'isAjax';
	const ARGUMENT_TYPE = 'type';
	const ARGUMENT_ACTION = 'action';
	
	const _VALUE_DEFAULT = 'default';
	const _VALUE_RENDER_VERSION_FLASH_MESSAGE = 'message';
	
	
	const _VALUE_CLASS_BUTTON = 'nj-button';
	const _VALUE_RENDER_TYPE_DEFAULT = 'div';

	protected $data = [];
	protected $extSettings = [];

	public function  initialize() {
		parent::initialize();
	}
	
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerUniversalTagAttributes();
		$this->registerArgument(self::ARGUMENT_CONTENT_DATA, 'array', 'Data array of the content element.',FALSE,NULL);
		$this->registerArgument(self::ARGUMENT_RENDER_TYPE, 'string', 'Tag name of the element.',FALSE,NULL);
	}
	

	protected function addWrapper($element,$type) {
		$wrap = $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_njbootstrap.']['settings.']['wrap.'][$element.'.'][$type];

		if($wrap === '1' ) {
			return true;
		} 
		return false;
	}
	
	
	/**
	 * @param string $argument Name of the array key in arguments
	 */
	protected function argumentGet($argument)
	{
		return $this->arguments[$argument];
	}
	
	/**
	 * @param string $argument Name of the array key in arguments
	 * @return boolean
	 */
	protected function argumentIsNotEmpty($argument)
	{
		if($this->argumentIsSet($argument))
		{
			if($this->argumentGet($argument) !== '')
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param string $argument Name of the array key in arguments
	 * @return boolean
	 */
	protected function argumentIsSet($argument)
	{
		if($this->argumentGet($argument) !== NULL)
		{
			return true;
		}
		return false; 
	}
	
	/**
	 * @param string $argument Name of the array key in arguments
	 * @return boolean
	 */
	protected function argumentIsTrue($argument)
	{
		if(is_bool($this->argumentGet($argument)))
		{
			if($this->argumentIsSet($argument))
			{
				if($this->argumentGet($argument) === TRUE)
				{
					return true;
				}
			}
		}
		else {
			if($this->argumentIsSet($argument))
			{
				if($this->argumentGet($argument) > 0)
				{
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @return array
	 */
	protected function getExtSettings() {
		
		return $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_njbootstrap.']['settings.'];
	}
	
	protected function setAdditionalDataAttributes($attributes) {
		foreach ($attributes as $dataAttributeKey => $dataAttributeValue) {
			$this->tag->addAttribute('data-' . $dataAttributeKey, $dataAttributeValue);
		}
		
	}
	
	protected function setTagName() {
		if($this->argumentIsNotEmpty(self::ARGUMENT_RENDER_TYPE)) {
			$this->tag->setTagName($this->argumentGet(self::ARGUMENT_RENDER_TYPE));
		} else {
			$this->tag->setTagName(self::_VALUE_RENDER_TYPE_DEFAULT);
		}
	}
	
	/**
	 * @param string $expansion
	 * @param boolean $prependEmptySpace
	 */
	protected function concatOutput($expansion, $prependEmptySpace = FALSE)
	{
		if($prependEmptySpace) { $this->output .= ' '; }
		$this->output .= $expansion;
	}
	
	protected function resetOutput() {
		$this->output = '';
	}
	
	
}