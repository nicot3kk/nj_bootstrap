<?php
namespace T3kk\NjBootstrap\ViewHelpers\Wrap;
use T3kk\NjBootstrap\Utility\HtmlBuilderUtility as HtmlBuilder;

class MessageViewHelper extends \T3kk\NjBootstrap\ViewHelpers\Wrap\AbstractViewHelper {
	
	const _ARG_MESSAGE			= 'message';
	const _ARG_TRANSLATE		= 'translate';
	
	const _MESSAGE_TYPE_ERROR	= 'error';
	const _MESSAGE_TYPE_INFO	= 'info';
	const _MESSAGE_TYPE_SUCCESS = 'success';
	
	public function __construct() {
		parent::__construct();
	}
	
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument(self::_ARG_MESSAGE, 'string', 'The message to be wrapped.',TRUE,NULL);
		$this->registerArgument(self::_ARG_TRANSLATE,'boolean','Message is translation key.',FALSE,TRUE);
		$this->registerArgument(self::_ARG_TYPE,'string','Message type (info, error ...).',FALSE,self::_MESSAGE_TYPE_INFO);
	}
	
	public function initialize() {
		parent::initialize();
		$this->extSettings = $this->getExtSettings();	
		if($this->argumentIsSet(self::ARGUMENT_CONTENT_DATA)) {
			$this->data = $this->argumentGet(self::ARGUMENT_CONTENT_DATA);
		} 
	}
	
	public function render() {
		return $this->renderMessage();
	}
	
	/**
	 * Output:
	 * <div class="vd-message" data-type="info">
	 *		<span>THE MESSAGE</span>
	 * </div>
	 * 
     * @return string
     */
    protected function renderMessage()
    {
		$this->output = HtmlBuilder::elementOpen('div', HtmlBuilder::_TAG_ACTION_OPEN);
		
		$this->concatOutput(HtmlBuilder::attribute(HtmlBuilder::_ATTR_CLASS, 
			$this->setClass()), true);
		
		$this->concatOutput(HtmlBuilder::attribute('data-type', 
			$this->arguments['type']), true);
		
		$this->output .= HtmlBuilder::elementOpen('div', HtmlBuilder::_TAG_ACTION_CLOSE);
		
		if($this->argumentIsTrue('translate')) {
			$this->concatOutput(
				\TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate($this->argumentGet('message'), 'nj_bootstrap')
			);		
		} else {
			$this->concatOutput($this->argumentGet('message'));
		}
	
		$this->output .= HtmlBuilder::elementClose('div');
		
        return $this->output;
    }
	
	protected function setClass() {
		$tmp = HtmlBuilder::_CLASS_PREFIX.'-message';
		return $tmp;
	}
}
