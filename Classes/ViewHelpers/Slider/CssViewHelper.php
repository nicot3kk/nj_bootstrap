<?php
namespace T3kk\NjBootstrap\ViewHelpers\Slider;
use T3kk\NjBootstrap\Domain\Model\Slide;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;

class CssViewHelper extends \T3kk\NjBootstrap\ViewHelpers\AbstractViewHelper {
	
	const ARGUMENT_SLIDES = 'slides';
	
	protected $output = '';
	
	/**
     * Initialize arguments
     */
    public function initializeArguments() {
        parent::initializeArguments();
        $this->registerArgument(self::ARGUMENT_SLIDES, 'TYPO3\CMS\Extbase\Persistence\Generic\QueryResult', 'All slides of the slider');
	}
	
	public function render() {
		$images = [];
		if($this->argumentIsNotEmpty(self::ARGUMENT_SLIDES)) {
			$i=0;
			foreach($this->argumentGet(self::ARGUMENT_SLIDES) as $slide) {
				if(is_a($slide,Slide::class)) {
					if(is_a($slide->getImage(),FileReference::class)) {
						$images[$i]['file'] = $slide->getImage();
						$images[$i]['id'] = $slide->getUid();
					}
				}
				$i++;
			}
		}
		if(!empty($images)) {
			$imageService = $this->getImageService();
			$imageService->renderResponsiveBackgroundImages(
				$images, 
				'.slide',
				'data-slide'
			);
		}
	}
}