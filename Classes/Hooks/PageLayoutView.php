<?php
namespace T3kk\NjBootstrap\Hooks;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * DESCRIPTION: 
 * 
 * @author Nico Jatzkowski <nico@t3kk.de>
 * @package T3kk
 * @subpackage nj_bootstrap
 */
class PageLayoutView implements \TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface 
{
	/**
	 * Preprocesses the preview rendering of a content element.
	 *
	 * @param PageLayoutView $parentObject Calling parent object
	 * @param boolean $drawItem Whether to draw the item using the default functionalities
	 * @param string $headerContent Header content
	 * @param string $itemContent Item content
	 * @param array $row Record row of tt_content
	 * @return void
	 */
	public function preProcess(\TYPO3\CMS\Backend\View\PageLayoutView &$parentObject, &$drawItem, &$headerContent, &$itemContent, array &$row) 
	{
		//if (substr($row['CType'], 0, 6) !== 'valid_') {
		//	return;
		//}
		
		if ($row['CType'] !== 'valid_contact') {
			return;
		}
		
		$drawItem = FALSE; 
	
		//$fluidTemplate = GeneralUtility::makeInstance('Valid\ValidEdelmanErgo\Utility\FluidTemplateUtility');
		//$fluidTemplate->setTemplatePathAndFilename(GeneralUtility::getFileAbsFileName(
		//	FluidTemplateUtility::_DEFAULT_DIR_TEMPLATES_BE . 'tt_content/Templates/'.$row['CType'].'.html'
        //));
		
		//$assignValues = [];
		
		
		
		    
	}
	
}