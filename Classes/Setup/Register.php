<?php
namespace T3kk\NjBootstrap\Setup;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\SignalSlot\Dispatcher;

class Register {
	
	/**
     * @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher
     */
    protected $signalSlotDispatcher;
	
	/**
     * @param Dispatcher $signalSlotDispatcher
     */
    public function __construct(Dispatcher $signalSlotDispatcher = null) {
        $this->signalSlotDispatcher = $signalSlotDispatcher ?: GeneralUtility::makeInstance(Dispatcher::class);
    }
	
	/**
	 * Executes the setup tasks if extension is installed.
	 *
	 * @param string|null $extname Installed extension name
	 */
	public function executeOnSignal( $extname = null )
	{
		/** @var $logger \TYPO3\CMS\Core\Log\Logger */
		$logger = GeneralUtility::makeInstance('TYPO3\\CMS\\Core\\Log\\LogManager')->getLogger(__CLASS__);

		$logger->debug(
			'NjBootstap installation ...',
			[
				'time' => time(),
			  'extname' => $extname,
			]
		);
		
		
		if( $extname === 'nj_bootstrap' ) {
			self::execute();
		}
	}
	
	public static function execute()
	{
		$registry = GeneralUtility::makeInstance(\T3kk\NjBootstrap\Service\GeneralService::class);
		$registry::registerIcons();
	}
}
