<?php
namespace T3kk\NjBootstrap\Service;

class Constants {
	
	const NJ_AJAX_PAGETYPE	= '6517243';
	const EXT_DOMAIN = 'T3kk';
	const EXT_KEY = 'tx_njbootstrap';
	const EXT_SHORT = 'njbootstrap';
	const EXT_LIST_TYPE	= 'njbootstrap_pi1';
	const EXT_NAMESPACE	= 'NjBootstrap';
	const EXT_PATH  = 'nj_bootstrap';
    const EXT_TITLE = 'njs Bootstrap Package';
	const EXT_LANG_FILE_FRONTEND = 'LLL:EXT:nj_bootstrap/Resources/Private/Language/locallang.xlf:';
	const EXT_LANG_FILE_BACKEND	= 'LLL:EXT:nj_bootstrap/Resources/Private/Language/locallang_be.xlf:';
	
}