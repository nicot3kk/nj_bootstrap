<?php
namespace T3kk\NjBootstrap\Service;
use T3kk\NjBootstrap\Service\GeneralService;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Fluid\View\StandaloneView;

class MailService {
	
	const TEST_MODE = true;
	
	const MAIL_DEVELOPMENT = "nico@t3kk.de";
    const MAIL_PRODUCTION = "nico@t3kk.de";
    const MAIL_SYSTEM = "system@t3kk.de";
	
	
	/**
	 * @param string $templateFile
	 * @param string $subject
	 * @param array $assignValues
	 * @return type
	 */
	public static function sendMail($templateFile,$subject,$assignValues)
	{
		$templatePathAndFilename = GeneralService::getAbsoluteTemplatePathAndFilename($templateFile);
		
		$mailView = GeneralService::getInstance(StandaloneView::class);		
		$mailView->setTemplatePathAndFilename($templatePathAndFilename);
		
		$mailView->assignMultiple($assignValues);
		
		$mailBody = $mailView->render();
		
		$mailTo = self::getMailAddress();
		$mailFrom = self::MAIL_SYSTEM;

		$messenger = GeneralService::getInstance(MailMessage::class);
	
		$mailSubject = $subject;

		$messenger->setTo($mailTo)
			->setFrom($mailFrom)
			->setSubject($mailSubject);

//		// Possible attachments here
//		//foreach ($attachments as $attachment) {
//		// $message->attach($attachment);
//		//}

		// Plain text example
//		$message->setBody($emailBody, ‘text/plain’);

//		// HTML Email
		$messenger->setBody($mailBody, 'text/html');
		$messenger->send();
		
		return $messenger->isSent();
	}
	
	
	private static function getMailAddress() {
		$mailAddress = self::TEST_MODE ? self::MAIL_DEVELOPMENT : self::MAIL_PRODUCTION;
		return $mailAddress;
	}
	
}
