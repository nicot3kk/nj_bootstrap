<?php
namespace T3kk\NjBootstrap\Service;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;

class GeneralService {
	
	const EXT_KEY = 'nj_bootstrap';
	
	const EXT_KEY_SHORT = 'njbootstrap';
	
	const TABLE_TT_CONTENT = 'tt_content';
	const TABLE_PAGES = 'pages';
	const TABLE_PAGES_LANGUAGE_OVERLAY = 'pages_language_overlay';
	
	const DOKTYPE_DATA = 250;
	
	const PAGETYPE_SYSTEM = 'sys';
	const PAGETYPE_WEB = 'web';
	
	const TYPEICON_STATUS_NOTINMENU = '-not-in-menu';
	const TYPEICON_STATUS_ROOT = '-root';
	
	
	/**
	 * @param string $selection
	 */
	public static function registerIcons($selection = null) {
		/**
		 * [0] identifier, [1] svg, [2] doktype (additional), [3] add -not-in-menu, [4] add -root
		 */
		$icons = [
			self::TABLE_PAGES => [
				['data','doktype_data',250,self::PAGETYPE_SYSTEM]
			],
			self::TABLE_TT_CONTENT => [
				['carousel','ce_carousel']
			]
		];
		
		$typeIconStates = [
			self::TYPEICON_STATUS_NOTINMENU,self::TYPEICON_STATUS_ROOT
		];
		
		foreach($icons as $key => $value) {
			$rockon = false; /* check if $selection is set, if not rock on anyway */
			if(is_string($selection)) {
				if($key === $selection) {
					$rockon = true;
				}
			} else {
				$rockon = true;
			}
			if($rockon) {
				foreach($value as $icon) {
					$identifier = $icon[0];
					$svg = $icon[1];
					$addStates = false;

					if(self::addTableTypeIconClasses($key)) {
						switch($key) {
							case self::TABLE_TT_CONTENT:
								$tcaType = 'nj_'.$identifier;
								$iconIdentifier = self::EXT_KEY_SHORT.'-ttcontent-'.$identifier;
								break;
							case self::TABLE_PAGES:
								$tcaType = $icon[2];
								$iconIdentifier = self::EXT_KEY_SHORT.'-pages-doktype-'.$identifier;
								if(isset($icon[3])&&$icon[3]===self::PAGETYPE_WEB) {
									$addStates = true;
								}
								break;
						}
						
						self::addIconToTypeIconClasses($key, $tcaType, $iconIdentifier);
						if($key === self::TABLE_PAGES) {
							self::addIconToTypeIconClasses(self::TABLE_PAGES_LANGUAGE_OVERLAY, $tcaType, $iconIdentifier);
						}
						if($addStates) {
							foreach($typeIconStates as $status) {
								self::addIconToTypeIconClasses($key,$tcaType.$status, $iconIdentifier.$status);
								if($key === self::TABLE_PAGES) {
									self::addIconToTypeIconClasses(self::TABLE_PAGES_LANGUAGE_OVERLAY,$tcaType.$status, $iconIdentifier.$status);
								}
							}
						}
					}
					else {
						$iconIdentifier = self::EXT_KEY_SHORT.'-'.$identifier;
					}
					
					self::registerIcon($iconIdentifier,$svg);
					if($addStates) {
						foreach($typeIconStates as $status) {
							self::registerIcon($iconIdentifier.$status,$svg.$status);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Checks if icon have to be added to a table tca (typeicon_classes)
	 * @param type $key
	 * @return boolean
	 */
	protected static function addTableTypeIconClasses($key) {
		if($key === self::TABLE_TT_CONTENT || $key === self::TABLE_PAGES) {
			return true;
		}
		return false;
	}
	
	/**
	 * @param string $iconIdentifier
	 * @param string $svg
	 */
	protected static function registerIcon($iconIdentifier,$svg) {
		
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		$iconRegistry->registerIcon(
			$iconIdentifier,
			\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
			['source' => 'EXT:'.self::EXT_KEY.'/Resources/Public/Icons/'.$svg.'.svg']
		);
	}
	
	/**
	 * @param string $table
	 * @param string $tcaType
	 * @param string $iconIdentifier
	 */
	protected static function addIconToTypeIconClasses($table,$tcaType,$iconIdentifier) {
		$GLOBALS['TCA'][$table]['ctrl']['typeicon_classes'][$tcaType] = $iconIdentifier;
	}
	
	
	public static function getInstance($instance) {
		$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
		return $objectManager->get($instance);
	}
	
	public static function getClassName($class) {
		$explodedClass = \explode("\\",$class);
		
		$needles = ["Controller","Repository"];
		
		foreach($needles as $needle) {
			if(strpos($class, $needle) !== FALSE) {
				$delimiter = $needle;
			}
		}
		return lcfirst(\explode($delimiter,$explodedClass[\count($explodedClass)-1])[0]);
	}
	
	/**
	 * @return string
	 */
	public static function generateToken() {
		return $this->randomChars(1).substr(time(),-10).$this->randomChars(1);
	}
	
	/**
	 * @param int $numberOfChars
	 * @return string
	 */
	public static function randomChars($numberOfChars) {
		$seed = str_split('ABCDEFGHJKLMNPQRSTUVWXYZ');
		shuffle($seed);
		$rand = '';
		$i = 0;
		if(is_numeric($numberOfChars)) {
			while($i<$numberOfChars) {
				shuffle($seed);
				$rand .= $seed[0];
				$i++;
			}
		}
		return $rand;
	}
	
	
	/**
	 * Return path and filename for a file
	 * respect *RootPaths and *RootPath	
	 *
	 * @param string $relativePathAndFilename e.g. Email/Name.html
	 * @param string $part "template", "partial", "layout"
	 * @return string
	 */
	public static function getAbsoluteTemplatePathAndFilename($relativePathAndFilename, $part = 'template') 
	{
		if(strpos($relativePathAndFilename,'.html') === FALSE) {
			$relativePathAndFilename = $relativePathAndFilename.'.html';
		}
		
		$configurationManager = self::getInstance(ConfigurationManager::class);
		$extbaseFrameworkConfiguration = $configurationManager->getConfiguration(
			\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK
		);

		$rootPaths = $extbaseFrameworkConfiguration['view'][$part . 'RootPaths'];
		
		if(!is_null($rootPaths) && !empty($rootPaths)) {
			foreach ($rootPaths as $path) {

				$absolutePath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($path);
				if (file_exists($absolutePath . $relativePathAndFilename)) {
					$absolutePathAndFilename = $absolutePath . $relativePathAndFilename;
				}
			}
		} else {
			$absolutePathAndFilename = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(
				$extbaseFrameworkConfiguration['view'][$part . 'RootPath'] . $relativePathAndFilename
			);
		}
		
		if (!is_null($absolutePathAndFilename)) {	
			return $absolutePathAndFilename;
		}

		return NULL;
	}
	
	
	
}