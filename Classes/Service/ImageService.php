<?php
namespace T3kk\NjBootstrap\Service;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;

class ImageService extends \TYPO3\CMS\Extbase\Service\ImageService {
	
	const MEDIA_QUERY_RENDER_TYPE_STANDARD = 'standard';
	const MEDIA_QUERY_RENDER_TYPE_2X = '2x';
	
	protected $mediaQueries = [
		480 => 480,
		960 => 1024,
		1024 => 1280,
		1280 => 1920
	];
	
	/**
	 * 
	 * @param type $src
	 * @param type $image
	 * @param type $treatIdAsReference
	 * @param string $url
	 * @return array
	 */
	public function getDimensions($src, $image, $treatIdAsReference) {
		$originalImage = $this->getImage($src, $image, $treatIdAsReference)->getOriginalFile();
		$imagePath = $this->getImagePath($originalImage);
		$file = $imagePath . $originalImage->getIdentifier();
		$imageInformation = getimagesize($file);
		$dimensions['width'] = $imageInformation[0];
		$dimensions['height'] = $imageInformation[1];
		return $dimensions;
	}
	
	/**
	 * @param \TYPO3\CMS\Core\Resource\File $image
	 * @return string $imagePath
	 */
	public function getImagePath(\TYPO3\CMS\Core\Resource\File $image) {
		$storage = $image->getStorage();
		$storageConfiguration = $storage->getConfiguration();

		$basePath =  $storageConfiguration['basePath'];
		if(substr($basePath, strlen($basePath)-1 ) === "/")
		{
			$basePath = \substr($basePath, 0, \strlen($basePath)-1 );
		}	
		return $basePath;
	}
	
	
	public function getSvgInline($source) {
        if (!file_exists($source)) {
            return '';
        }
        $svgContent = file_get_contents($source);
		$svgContent = preg_replace('/<script[\s\S]*?>[\s\S]*?<\/script>/i', '', $svgContent);
	
        $previousValueOfEntityLoader = libxml_disable_entity_loader(true);
        $svgElement = simplexml_load_string($svgContent);
        libxml_disable_entity_loader($previousValueOfEntityLoader);
        // remove xml version tag
        $domXml = dom_import_simplexml($svgElement);
		
		$svg = $domXml->ownerDocument->saveXML($domXml->ownerDocument->documentElement);
		return preg_replace( "/(width|height)=\".*?\"/","", $svg );

        
		
		//$domXml->ownerDocument->saveXML($domXml->ownerDocument->documentElement);
    }
	
	
	
	
	protected function removeAttribute($svgString) {
		$doc = new \DOMDocument();
		$doc->loadXML( $svgString );
		$xpath = new \DOMXPath($doc);
		$rootNamespace = $doc->lookupNamespaceUri($doc->namespaceURI);
		$xpath->registerNamespace('svg', $rootNamespace); 
		$paths = @$xpath->query( "//svg");
		foreach($paths as $node) {
			$nodes[] = $node;
		  }
		\TYPO3\CMS\Extbase\Utility\DebuggerUtility::var_dump($nodes, __CLASS__ . '::' . __FUNCTION__);
	} 
	
	
	
	
	/**
	 * $images = [
	 *		'file' => \TYPO3\CMS\Extbase\Domain\Model\FileReference (the image),
	 *		''id => int (uid, identifier for attribute [data-uid])
	 * ]
	 * 
	 * @param array $fileReferences
	 * @param string $cssSelector
	 * @param string $attr
	 * @paran string $attrValue
	 */
	public function renderResponsiveBackgroundImages($fileReferences = NULL, $cssSelector = 'body') {
		
		$css = '';
		foreach($fileReferences as $fileReference) {
			foreach($this->mediaQueries as $key => $value) {
				if(is_a($fileReference, \TYPO3\CMS\Extbase\Domain\Model\FileReference::class) ||
					is_a($fileReference, \TYPO3\CMS\Core\Resource\FileReference::class)) {
					$image = $this->getImage($fileReference->getUid(), NULL, true);
					$processedImage = $this->applyProcessingInstructions($image, ["maxWidth" => $value]);
					$processedImage2X = $this->applyProcessingInstructions($image, ["maxWidth" => ($value * 2)]);
				}

				if(!is_null($processedImage)) {
					$uri = $this->getImageUri($processedImage);
				}
				if(!is_null($processedImage2X)) {
					$uri2X = $this->getImageUri($processedImage2X);
				}
				
				$cssContainer = $cssSelector;
				$css .= $this->renderMediaQueries($uri,$key, $cssContainer);
				$css .= $this->renderMediaQueries($uri2X,$key, $cssContainer,self::MEDIA_QUERY_RENDER_TYPE_2X);
				
			}
		}
		$this->getPageRenderer()->addCssInlineBlock(
			'backgroundImages-'.GeneralService::randomChars(5),
			$css,
			false,
			true
		);
	}
	
	/**
	 * @param string $uri
	 * @param int $queryWidth
	 * @param string $cssSelector
	 * @param string $type
	 */
	protected function renderMediaQueries($uri,$queryWidth, $cssSelector = 'body', $type = self::MEDIA_QUERY_RENDER_TYPE_STANDARD) {
		$mediaQuery = '';
		switch($type) {
			case self::MEDIA_QUERY_RENDER_TYPE_STANDARD:
				$mediaQuery = "@media screen and (min-width:".$this->setUnit($queryWidth).") { "
					. $cssSelector
					. " { background-image:url('".$uri."'); } } ";
				break;
			case self::MEDIA_QUERY_RENDER_TYPE_2X:
				$mediaQuery = "@media "
					. "screen and (min-width:".$this->setUnit($queryWidth).") and (-webkit-min-device-pixel-ratio: 2), "
					. "screen and (min-width:".$this->setUnit($queryWidth).") and (min-resolution: 192dpi), "
					. "screen and (min-resolution: 2dppx) { "
					. $cssSelector
					. " { background-image:url('".$uri."'); } } ";
				break;
		}
		return $mediaQuery;
	}
	
	protected function setUnit($value,$unit='px') {
		return $value.$unit;
	}
	
	/**
	 * Provides a shared (singleton) instance of PageRenderer
	 * @return \TYPO3\CMS\Core\Page\PageRenderer
	 */
	protected function getPageRenderer() {
		return \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Page\PageRenderer::class);
	}
}
