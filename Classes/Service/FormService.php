<?php
namespace T3kk\NjBootstrap\Service;
use T3kk\NjBootstrap\Domain\Repository\FormRepository;
use T3kk\NjBootstrap\Service\GeneralService;

class FormService {
	
	/**
	 * @var array
	 */
	public static $formFields = [
		'country' => [
			'additionalArguments' => ['autocomplete' => 'off', 'spellcheck' => 'false'],
			'addon-class' => 'flag',
			'autocomplete' => 'off',
			'fieldset' => 'adressData',
			'mandatory' => false,
			'type' => 'input',
		],
		'email'=> [
			'addon-class' => 'envelope',
			'autocomplete' => 'off',
			'eval' => 'email',
			'fieldset' => 'contactData',
			'mandatory' => false,
			'type' => 'input',
		],
		'firm' => [
			'additionalArguments' => ['autocomplete' => 'off', 'spellcheck' => 'false'],
			'autocomplete' => 'off',
			'fieldset' => 'adressData',
			'mandatory' => false,
			'type' => 'input',
		],
		'firstName' => [
			'additionalArguments' => ['autocomplete' => 'off', 'spellcheck' => 'false'],
			'addon-class' => 'user',
			'autocomplete' => 'off',
			'fieldset' => 'adressData',
			'mandatory' => false,
			'type' => 'input',
		],
		'lastName' => [
			'additionalArguments' => ['autocomplete' => 'off', 'spellcheck' => 'false'],
			'addon-class' => 'user',
			'autocomplete' => 'off',
			'fieldset' => 'adressData',
			'mandatory' => false,
			'type' => 'input',
		],
		'location' => [
			'additionalArguments' => ['autocomplete' => 'off', 'spellcheck' => 'false'],
			'addon-class' => 'map-signs',
			'autocomplete' => 'off',
			'fieldset' => 'adressData',
			'mandatory' => false,
			'type' => 'input',
		],
		'message' => [
			'eval' => ['size' => 50],
			'fieldset' => 'messageData',
			'mandatory' => false,
			'type' => 'text',

		],
		'mobile'=> [
			'addon-class' => 'mobile',
			'autocomplete' => 'off',
			'fieldset' => 'contactData',
			'mandatory' => false,
			'type' => 'input',
		],
		'name' => [
			'additionalArguments' => ['autocomplete' => 'off', 'spellcheck' => 'false'],
			'addon-class' => 'user',
			'autocomplete' => 'off',
			'fieldset' => 'adressData',
			'mandatory' => false,
			'type' => 'input',
		],
		'phone'=> [
			'addon-class' => 'phone',
			'autocomplete' => 'off',
			'fieldset' => 'contactData',
			'mandatory' => false,
			'type' => 'input',
		],
		'street' => [
			'additionalArguments' => ['autocomplete' => 'off', 'spellcheck' => 'false'],
			'addon-class' => 'home',
			'autocomplete' => 'on',
			'fieldset' => 'adressData',
			'mandatory' => false,
			'type' => 'input',
		],
		'subject' => [
			'additionalArguments' => ['autocomplete' => 'off', 'spellcheck' => 'false'],
			'addon-class' => 'pencil',
			'autocomplete' => 'off',
			'fieldset' => 'messageData',
			'mandatory' => false,
			'type' => 'input',

		],
		'zipCode' => [
			'additionalArguments' => ['autocomplete' => 'off', 'spellcheck' => 'false'],
			'addon-class' => 'home',
			'mandatory' => false,
			'type' => 'input',
			'fieldset' => 'adressData'
		],
		'url' => [
			'additionalArguments' => ['autocomplete' => 'off', 'spellcheck' => 'false'],
			'addon-class' => 'globe',
			'eval' => 'url',
			'mandatory' => false,
			'type' => 'input',
			'fieldset' => 'contactData'
		],
	];
	
	
	/**
	 * @param int $lockDuration The time in Minutes to lock IP for new form fullfillment
	 * @return boolean
	 */
	public static function floodProtection($lockDuration = 3) {
		$lockDuration = $lockDuration * 60;
		$ip = $_SERVER['REMOTE_ADDR'];
		$formRepository = GeneralService::getInstance(FormRepository::class);
		
		$formerlyEntriesByIp = $formRepository->findByLogIp($ip)->toArray();
		
		if(count($formerlyEntriesByIp)) {
			$creationDate = $formerlyEntriesByIp[0]->getCrdate();
			if(is_int($creationDate)  && (time() - $creationDate) > ($lockDuration)) {
				return true;
			}
		} else {
			return true;
		}
		return false;
	}
	
}
