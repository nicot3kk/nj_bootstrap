<?php
namespace T3kk\NjBootstrap\Controller;


class FormController extends \T3kk\NjBootstrap\Controller\AbstractController {
	
	protected $contentObj;
	protected $data;


	/**
	 * Initializes the controller before invoking an action method.
	 * @return void
	 */
	protected function initializeAction() {
		parent::init('Form');
		$this->contentObj = $this->configurationManager->getContentObject();
		$this->data = $this->contentObj->data;
	}
	
	public function renderAction() {
		$assignValues = [];
		$assignValues['data'] = $this->data;
		$this->view->assignMultiple($assignValues);
	}
	
	/**
	 * @return void
	 */
	public function contactAction() {
		$assignValues = [];
		$this->view->assignMultiple($assignValues);
	}
	
}
