<?php
namespace T3kk\NjBootstrap\Controller;
use T3kk\NjBootstrap\Domain\Model\Form;
use T3kk\NjBootstrap\Service\FormService;
use T3kk\NjBootstrap\Service\GeneralService;
use T3kk\NjBootstrap\Service\MailService;


/**
 * Description of ContactController
 *
 * @author nico-jatzkowski
 */
class ContactController extends \T3kk\NjBootstrap\Controller\AbstractController {
	
	const FORM_FIELDS_DEFAULT = 'name,email,message';
	
	const FORM_FIELDS_MANDATORY = 'name,email,message';
	
	/**
	 * @var array 
	 */
	protected $cSettigns;
	
	protected $contentObj;
	
	/**
	 * @var array 
	 */
	protected $data;

	/**
	 * @var array 
	 */
	protected $errors = [];
	
	/**
	 * @var \T3kk\NjBootstrap\Domain\Repository\FormRepository
	 * @inject
	 */
	protected $formRepository = NULL;
	
	/**
	 * @var boolean 
	 */
	protected $success = false;

	
	/**
	 * Initializes the controller before invoking an action method.
	 * @return void
	 */
	protected function initializeAction() {
		$controller = GeneralService::getClassName(__CLASS__);
		parent::init($controller);
		$this->contentObj = $this->configurationManager->getContentObject();
		$this->data = $this->contentObj->data;
		$this->cSettings = $this->getControllerSettings($controller);
	}
	
	
	/**
	 * @return void
	 */
	public function formAction() {
		$action = explode("Action", __FUNCTION__)[0];
		$assignValues = ['data' => $this->data];
		
		$success = false;
		$fields = [];
		$errors = [];
		$options = [];
		
		$selectedFields = explode(',',isset($this->cSettings['fields']['default']) ? $this->cSettings['fields']['default'] : self::FORM_FIELDS_DEFAULT);
		$mandatory = explode(',',isset($this->cSettings['fields']['mandatory']) ? $this->cSettings['fields']['mandatory'] : self::FORM_FIELDS_MANDATORY);
		
		$options['useFieldsets'] = false;
		$options['useIcons'] = isset($this->cSettings['useIcons']) && $this->cSettings['useIcons'] == 1 ? true : false; 
		
		$options['ajax'] = true;
		
		$fieldDefinitions = FormService::$formFields;
		
		foreach($selectedFields as $field) {
			if(array_key_exists($field, $fieldDefinitions)) {
				$fields[$field] = $fieldDefinitions[$field];
				if(in_array($field,$mandatory)) {
					$fields[$field]['mandatory'] = true;
				}
			}
		}
		
		
		
		
//		
//		$fields = [];
//		
//		foreach(explode(',',$selection) as $field)
//		{
//			if(is_array($fieldDefinitions))
//			{
//				if(array_key_exists($field, $fieldDefinitions))
//				{
//					$fields[$field] = $fieldDefinitions[$field];
//				
//					if(in_array($field,explode(',',$mandatory)))
//					{
//						$fields[$field]['mandatory'] = true;
//					}
//				}
//			}
//		}
//		
//		$generateForm = true;
//		
//		$formData = [];
//		if($this->request->hasArgument('submitFormData'))
//        {
//			$formData = $this->request->getArguments();
//			
//			$errors = $this->checkFormData($mandatory,$selection,$fieldDefinitions,$formData);
//			
//			if(!empty($errors))
//			{
//				$assignValues['errors'] = $errors;
//			}
//			else
//			{
//				if($this->sendMail($formData))
//				{
//					$formData['success'] = 1;
//					$generateForm = false;
//					$success = true;
//				}
//			}
//			
//			$assignValues['formData'] = $formData;
//		}
//		
//		if($generateForm)
//		{
//			$assignFields = [];
//			foreach($fields as $key => $value)
//			{
//				$assignFields[$value['fieldset']][$key] = $value;
//			}
//			$assignValues['fields'] = $assignFields;
//		}
//		
//		if($success)
//		{
//			$this->completeForm($formData,$selection);
//		}
//		
		//completion
		
		$assignValues['form'] = [
			'fields' => $fields,
			'options' => $options,
			'success' => $success
		];
		if(!empty($errors)) {
			$assignValues['form']['errors'] = $errors;
		}
		$this->view->assignMultiple($assignValues);
	}
	
	public function submitAction() {
		
		$status = FormService::floodProtection(0);
		
		$assignValues = [];
		$args = [];
		
		if($status) {
			foreach($this->request->getArguments() as $key => $value) {
				if($this->isFormArgument($key)) {
					$args[$this->splitFormArgument($key)] = $value;
				}
			}
			
			$assignValues['arguments'] = $args;
			
			if(count($args) > 0) {
				$form = $this->addFormToRepository($args);
			}
			
			$subject = 'Kontaktformular';
			$this->success = MailService::sendMail('Contact/Mail',$subject,$assignValues);
		}
		
		$assignValues['errors'] = $this->errors;
		$this->view->assignMultiple($assignValues);
		
		return json_encode( 
            [
				"content" => $this->view->render(),
                "success" => $this->success,
			]
        );
	}
	
	
	
	/**
	 * @param array $args
	 * @return \T3kk\NjBootstrap\Domain\Model\Form
	 */
	protected function addFormToRepository($args) {
		$storagePid = $this->settings['persistence']['model']['form'];
		
		$form = new Form();
		if(!is_null($storagePid)) { $form->setPid($storagePid); }

		foreach($args as $key => $value) {
			$func = 'set'.ucfirst($key);
			$form->$func($value);
		}
		$form->setLogIp($_SERVER['REMOTE_ADDR']);
		$this->formRepository->add($form);
		$this->persistenceManager->persistAll();
		return $form;
	}
	
	
	
	protected function isFormArgument($key) {
		$keyIdentifier = 'form_';
		if(\strpos($key,$keyIdentifier) !== false) {
			return true;
		}
		return false;
	}
	
	
	protected function splitFormArgument($key) {
		$keyIdentifier = 'form_';
		return str_replace($keyIdentifier, '', $key);
	}
}
