<?php
namespace T3kk\NjBootstrap\Controller;

class AbstractController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	
	/**
	 * @var int
	 */
	protected $storagePid;
	
	
	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
	 * @inject
	 */
	protected $configurationManager;
	
	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 * @inject
	 */
	protected $persistenceManager;
	
	
	
	/**
	 * 
	 * @param string $controller
	 */
	protected function init($controller) {	
		$this->checkDependencies($controller);
		
		$this->rebaseSettings();
		
		$this->setStoragePid();
		
	}	
	
	/**
	 * @param type $controller
	 * @throws \TYPO3\CMS\Extbase\Configuration\Exception
	 */
	protected function checkDependencies($controller) {
		if($controller !== null) {
			
		}
		else {
			throw new \TYPO3\CMS\Extbase\Configuration\Exception('Kein Controller übergeben. Überprüfe die Controller-Klassen.',48246892768209576);
		}
		
		if(!isset($this->settings)) {
			throw new \TYPO3\CMS\Extbase\Configuration\Exception('Please include typoscript to enable the extension.', 48246892768209576 );
		}
	}
	
	/**
	 * @throws \Exception
	 */
	protected function callActionMethod() {
		Try {
			parent::callActionMethod();
		} Catch(\Exception $e) {
			$this->response->appendContent($e->getMessage());
		}
	}
	
	protected function getCaller() {
		$trace = \debug_backtrace();
		$name = $trace[2]['function'];
		return empty($name) ? 'global' : $name;
	}
	
	
	protected function rebaseSettings() {
		if(isset($this->settings['flexform']['general']['typoScript']) && (int)$this->settings['flexform']['general']['typoScript'] === 0) {
			$this->settings = \array_merge_recursive($this->settings, $this->settings['flexform']);
		} 
		unset($this->settings['flexform']);
		
		//add persistence settings
		$settings = $this->configurationManager->getConfiguration(
			\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT,
			'tx_njbootstrap'
		);
		if(isset($settings['plugin.']['tx_njbootstrap.']['persistence.'])) {
			$pids = $settings['plugin.']['tx_njbootstrap.']['persistence.'];
			foreach($pids as $key => $value) {
				$this->settings['persistence'][substr($key, 0, -1)] = $value;
			}
		}
	}
	
	
	protected function storagePidIsset()
	{
		if(isset($this->settings['persistence']['storagePid']))
		{
			return true;
		}
		else {
			return false;
		}
	}
	
	protected function setStoragePid() {
		if(!$this->storagePidIsset()) {
			$this->contentObj = $this->configurationManager->getContentObject();
			$this->settings['persistence']['storagePid'] = $this->contentObj->data['pages'];
		}
	}
	
	/**
	 * @param string $controller
	 * @return array
	 */
	protected function getControllerSettings($controller) {
		if(isset($this->settings['controller'][$controller])) {
			return $this->settings['controller'][$controller];
		}
		return NULL;
	}
	
}
