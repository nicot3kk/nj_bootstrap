<?php
if(!defined('TYPO3_MODE')) die ('Access denied.');

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF'][$_EXTKEY] = unserialize($_EXTCONF);


\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'T3kk.'.$_EXTKEY,
    'Pi1',
    [
		'Backend' => 'tca',
	],
    // non-cacheable actions
    [
		'Backend'=>'tca',
	]
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'T3kk.'.$_EXTKEY,
    'Contact',
    [
		'Contact' => 'address,compact,extended,socialMedia',
	],
    // non-cacheable actions
    [
		'Contact' => 'form,submit',
	]
);


/**
 * PageTSConfig
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TSconfig/Mod/ContentElement/NewContentElementWizard.t3s">'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/TSconfig/Mod/WebLayout/BackendLayouts.t3s">'
);



if(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('gridelements'))
{
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
		'<INCLUDE_TYPOSCRIPT: source="FILE:EXT:' . $_EXTKEY . '/Configuration/PageTS/Mod/WebLayout/Gridelements.t3s">'
	);
}

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][$_EXTKEY] = 'EXT:'.$_EXTKEY.'/Classes/Hooks/PageLayoutView.php:T3kk\NjBootstrap\Hooks\PageLayoutView';

//TODO MULTILANGUAGE
#$GLOBALS['TYPO3_CONF_VARS']['FE']['addRootLineFields'] .= ',tx_realurl_pathsegment,tx_realurl_exclude,tx_realurl_pathoverride';
#$GLOBALS['TYPO3_CONF_VARS']['FE']['pageOverlayFields'] .= ',tx_realurl_pathsegment';


/**
 * Cache
 */

// Define state cache, if not already defined
//if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$_EXTKEY])) {
//   $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$_EXTKEY] = [
//        'frontend' => 'TYPO3\\CMS\\Core\\Cache\\Frontend\\VariableFrontend',
//        'backend' => 'TYPO3\\CMS\\Core\\Cache\\Backend\\Typo3DatabaseBackend',
//    ];
//}


/**
 * Execute the setup tasks automatically
 */
if (TYPO3_MODE === 'BE') {
//	$class = 'TYPO3\\CMS\\Extbase\\SignalSlot\\Dispatcher';
//	$dispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance($class);
//	
//	$dispatcher->connect(
//		'TYPO3\\CMS\\Extensionmanager\\Service\\ExtensionManagementService',
//		'hasInstalledExtensions',
//		'T3kk\\NjBootstrap\\Setup\\Register',
//		'executeOnSignal'
//	);
	
	
	$GLOBALS['TYPO3_CONF_VARS']['LOG']['writerConfiguration'] = array(
    // configuration for ERROR level log entries
  \TYPO3\CMS\Core\Log\LogLevel::ERROR => array(
      // add a FileWriter
    'TYPO3\\CMS\\Core\\Log\\Writer\\FileWriter' => array(
        // configuration for the writer
      'logFile' => '/var/www/public/t3syslog.log'
    )
  )
);
	
	/** @var $logger \TYPO3\CMS\Core\Log\Logger */
//		$logger = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Log\LogManager')->getLogger(__CLASS__);
//
//		$logger->info(
//			'NjBootstap installation ...',
//			[
//				'time' => time(),
//			  'say','hello'
//			]
//		);
//		$logger->debug('NjBootstap installation (debug) ...', 
//			[
//				'time' => time(),
//				'say','hello'
//			]);
		
	/** 
	 * @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher 
	 */
//	$dispatcherClass = 'TYPO3\\CMS\\Extbase\\SignalSlot\\Dispatcher';
//	$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance($dispatcherClass);
//	$signalSlotDispatcher->connect(	
//		'TYPO3\\CMS\\Extensionmanager\\Utility\\InstallUtility', 
//		'afterExtensionInstall', 
//		'T3kk\\NjBootstrap\\Setup\\Register', 
//		'executeOnSignal', TRUE);

	
	

}



$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_njbootstrap_contact[form_name]';
$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_njbootstrap_contact[form_email]';
$GLOBALS['TYPO3_CONF_VARS']['FE']['cacheHash']['excludedParameters'][] = 'tx_njbootstrap_contact[form_message]';
