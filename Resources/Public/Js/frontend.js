var sel_slider_rhinoSlider = '.ce[data-ctype="nj_carousel"] .nj-slider[data-slider="rhinoSlider"]';
var sel_ce_nj_maps = '.ce[data-ctype="nj_maps"]';

var sel_el_nj_button = '.nj-button[data-action]';


var NJ_BOOTSTRAP = function() {};

NJ_BOOTSTRAP.prototype.arrayLength = function(arr) {
	var count = 0;
	for (var k in arr) {
	  if (arr.hasOwnProperty(k)) {
		count++;
	  }
	}
	return count;
};


NJ_BOOTSTRAP.prototype.empty = function(arr) {
	if(this.arrayLength(arr) > 0) {
		return false;
	} 
	return true;
};

window.addEventListener('DOMContentLoaded', function() {
	
	var njBootstrap = new NJ_BOOTSTRAP();
	
	if(requirejsLoaded()) {
		requirejs(['modernizr'], function(Modernizr) {
			//do some great stuff
		});
		
		requirejs(['jquery'], function($) {
			//add animateCss to jquery
			$.fn.extend({
				animateCss: function (animationName,removeAninimatedStatus=true) {
					var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
					$(this).addClass('animated ' + animationName).one(animationEnd, function () {
						if(removeAninimatedStatus) {
							$(this).removeClass('animated ' + animationName);
						}
					});
				}
			});
			
			$.fn.isOnScreen = function() {
				var win = $(window);

				var viewport = {
					top : win.scrollTop(),
					left : win.scrollLeft()
				};
				viewport.right = viewport.left + win.width();
				viewport.bottom = viewport.top + win.height();

				var bounds = this.offset();
				bounds.right = bounds.left + this.outerWidth();
				bounds.bottom = bounds.top + this.outerHeight();

				return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

			};
			
			requirejs(['jqueryDebounce'], function() {
				$(window).scroll($.debounce( 250, true, function(){
					if($('body').attr('data-on-nav-scrolling') === undefined || $('body').attr('data-on-nav-scrolling') === '0') {
						$('body').addClass('on-scroll');
						$('body > header').removeClass('pinned');
					}
				}));
				$(window).scroll($.debounce( 250, function(){
					if($('body').attr('data-on-nav-scrolling') === undefined || $('body').attr('data-on-nav-scrolling') === '0') {
						$('body').removeClass('on-scroll');
						if($('body').scrollTop() > $(window).height()) {
							$('body > header').addClass('pinned');
						}
					}
				}));
			});
			
			
			$(window).scroll(function() {
				if($('body').attr('data-on-nav-scrolling') === undefined || $('body').attr('data-on-nav-scrolling') === '0') {
					$('body').addClass('on-scroll');
					$('.ce').each(function() {
						if($(this).isOnScreen()) {
							//fade in color sheme (add class out of data-attribute + css transition)
							if($(this).attr('data-color-sheme')) {
								ceAddColorSheme($(this));
							}
							
							if($(this).attr('data-ctype') !== undefined) {
								if($(this).attr('data-ctype') === 'nj_textimage') {
									var animation = $(this).find('.nj-flex[data-animate-css="1"]').attr('data-animation');
									$(this)
										.find('.nj-flex[data-animate-css="1"] .nj-flex-item.nj-textimage--image')
										.not('.animated') 
										.animateCss(animation,false);
								}
							}		
						}
					});
				}
				
				$('[data-animate-this="1"]').each(function() {
					var animation;
					if($(this).attr('data-animate-css') && $(this).attr('data-animation')) {
						animation = $(this).attr('data-animation');
					} else {
						animation = $(this).parents('[data-animate-css="1"]').attr('data-animation');
					}
					
					if(varIsset(animation)) {
						if($(this).isOnScreen()) {
							if(!$(this).hasClass('animated')) {
								$(this).animateCss(animation,false);
							}
						}
					}
				});
				
				
			});
			
			$(window).resize(function(){
				var getslidewd= $(window).width();
				var getslidehi= $(window).height();
				$('.rhino-container').width(getslidewd);
				$('.rhino-container').height(getslidehi);
				$('.rhino-container .slide').width(getslidewd);
				$('.rhino-container .slide').height(getslidehi);
			});
			
			
			if($(sel_slider_rhinoSlider).length > 0) {
				requirejs(['rhinoSlider'], function() {
					$(sel_slider_rhinoSlider+' .slider-container').each(function(){
						
						$(this).rhinoslider({
							showBullets: 'never',
							showControls: 'hover',
							easing: $(this).attr('data-easing'),
							effect: $(this).attr('data-effect'),
							effectTime: $(this).attr('data-time-effect'),
							autoPlay: boolean($(this).attr('data-autoplay')),
							randomOrder:false,
							controlsMousewheel:false,
							controlsKeyboard:true,
							controlsPrevNext:true,
							controlsPlayPause:false,
							pauseOnHover: boolean($(this).attr('data-pause-on-hover')),
							animateActive:true,
							cycled:true,
							showTime:$(this).attr('data-time-show'),
						});
					
						var getslidewd= $(window).width();
						var getslidehi= $(window).height();
						$('.rhino-container').width(getslidewd);
						$('.rhino-container').height(getslidehi);
						$('.rhino-container .slide').width(getslidewd);
						$('.rhino-container .slide').height(getslidehi);
						
					});
				});
			}
			
		});
		
		requirejs(["jquery", "velocity", "velocityUI"],function($,Velocity) {
			$(function() {
		
				$('nav a').bind('click', function(event) {
					event.preventDefault();
					event.stopPropagation();
					
					var $anchor = $(this);
					var selector = $anchor.attr('href');
					$(selector)
						.velocity('stop')
						.velocity("scroll", {
							easing:'easeInOutQuint', 
							duration:1250,
							begin: function (event) {
								$(selector)
									.find('.nj-flex[data-animate-css="1"] .nj-flex-item.nj-textimage--image')
									.not('.animated') 
									.css('opacity',0);
								$('body').attr('data-on-nav-scrolling',1)
							},
							complete: function (event) {
								var animation = $(selector).find('.nj-flex[data-animate-css="1"]').attr('data-animation');
								$(selector)
									.find('.nj-flex[data-animate-css="1"] .nj-flex-item.nj-textimage--image')
									.not('.animated') 
									.css('opacity',1)
									.animateCss(animation,false);
							
								if($(selector).attr('data-color-sheme')) {
									ceAddColorSheme($(selector));
								}
							
								$('body').attr('data-on-nav-scrolling',0);
							},
					});
				});
			});
		});
	}
});


window.addEventListener('DOMContentLoaded', function() {
	if(requirejsLoaded()) {
		requirejs(['jquery','njForm'], function($) {
			var njBootstrapForm = new NJ_BOOTSTRAP_FORM();
			var inputs = 'input[type="text"],input[type="email"],input[type="url"],textarea';
			
			$(inputs).on('input',function() {
				njBootstrapForm.checkFormField($(this));
			});
			
			//$(inputs).change(function() {
			//	njBootstrapForm.checkFormField($(this));
			//});
			
		});
	}
});


/**
 * element : nj-button
 */
window.addEventListener('DOMContentLoaded', function() {
	if(requirejsLoaded()) {
		requirejs(['jquery','njForm','njAjax'], function($) {
			var njBootstrap = new NJ_BOOTSTRAP();
			var njBootstrapAjax = new NJ_BOOTSTRAP_AJAX();
			var njBootstrapForm = new NJ_BOOTSTRAP_FORM();
			
			$(document).on('click',sel_el_nj_button,function(e) {
				
				if($(this).attr('data-is-ajax') === '1') {
					e.preventDefault();
	
					var params = [];
					
					if($(this).not(':disabled')) {
						
						switch($(this).attr('data-action')) {
							case njBootstrapAjax._AJAX_ACTION_SUBMIT_FORMDATA:
								var container = $(this).parents('.ce[data-uid]');
								//var errors = njBootstrapForm.checkFormData(container);
								params = njBootstrapForm.getFormData(container);
								params['ce'] = container.attr('data-uid');
								params['plugin'] = 'contact';
								params['action'] = njBootstrapAjax._AJAX_ACTION_SUBMIT_FORMDATA;
								params['controller'] = 'Contact';
								console.log(params);
								njBootstrapAjax.ajaxHandler(params,false);
								break;
						}
					}
					
				} else {
					if($(this).attr('data-action') === 'showForm') {
						btnActionShowForm();
					}
				}
			});
		});
	}
});

/**
 * ce : nj_maps
 */
window.addEventListener('DOMContentLoaded', function() {
	if(requirejsLoaded()) {
		requirejs(['jquery'], function($) {
			if($(sel_ce_nj_maps).length > 0) {
				
				var maps = new Array();
				var marker = new Array();
				
				$(sel_ce_nj_maps).each(function() {
					
					var uid = $(this).attr('data-uid');
					var container = $(this);
					var apiKey = container.find('.nj-googlemaps').attr('data-gm-api-key');
					
					require([
						'async!https://maps.googleapis.com/maps/api/js?key='+apiKey,
						'njGooglemaps'
					], function() {
						njGooglemapsInit(container,uid,maps,marker);
					});
				});
			}
		});
	}
});


//ScrollMagic

//window.addEventListener('DOMContentLoaded', function() {
//	if(requirejsLoaded()) {
//		requirejs(['scrollMagic'], function(ScrollMagic) {
//			
//			var controller = new ScrollMagic.Controller({
//					
//				});
//			
//			new ScrollMagic.Scene({
//				triggerElement:'#main',
//				reverse:true,
//				triggerHook: 1
//				})
//				.setClassToggle("header", "pinned")
//				.addTo(controller);
//			
//		});
//	}
//});




function btnActionShowForm() {
	showOverlay();
}


function showOverlay(effect) {
	if(effect == undefined) { effect = 'bounceInLeft'; }
	
	$('body > .overlay').css('display','block').animateCss(effect,false);
}

function requirejsLoaded() {
	if (typeof(requirejs) !== "undefined") {
		return true;
	}
	return false;
}

function boolean($attribute) {
	if($attribute === '1' || $attribute === 1 || $attribute == "true" || $attribute === true) {
		return true;
	} 
	return false;
}

function ceAddColorSheme(selector) {
	$(selector).addClass($(selector).attr('data-color-sheme'));
}

function varIsset(checkVar) {
	if(checkVar !== '' && checkVar !== undefined && checkVar !== 'undefined') {
		return true;
	}
	return false;
}


