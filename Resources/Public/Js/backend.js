requirejs( [
	'jquery'
],function($) {
	
	//add animateCss to jquery
	$.fn.extend({
		animateCss: function (animationName) {
			var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
			$(this).addClass('animated ' + animationName).one(animationEnd, function () {
				$(this).removeClass('animated ' + animationName);
			});
		}
	});
	
	function njAnimateCssInit(uid,animationInputSelector) {
		
		$('.container-image')
			.width($('.container-image IMG').outerWidth(true))
			.height($('.container-image IMG').outerHeight(true));
		
		var selAnimationClass = $('input[name="data[tt_content]['+uid+'][nj_animation]"').val();
		if(selAnimationClass === 'undefined' || selAnimationClass === undefined || selAnimationClass === '') {
			$('input[name="data[tt_content]['+uid+'][nj_animation]"').val($(animationInputSelector).val());
		} else {
			$(animationInputSelector).val($('input[name="data[tt_content]['+uid+'][nj_animation]"').val());
		}
	}
	
	function njAnimateCssUpdateInput(uid,selectedAnimation) {
		$('input[data-formengine-input-name="data[tt_content]['+uid+'][nj_animation]"]')
			.val(selectedAnimation);
		$('input[name="data[tt_content]['+uid+'][nj_animation]"')
			.val(selectedAnimation);
	};
	
	function njGetLocationData(container,uid) {
		
		var street	= njGetInputFieldValue(container,uid,'street');
		var zipCode	= njGetInputFieldValue(container,uid,'zip_code');
		var city	= njGetInputFieldValue(container,uid,'city');
		var country	= njGetInputFieldValue(container,uid,'country');
		var title	= njGetInputFieldValue(container,uid,'title');
		
		var address = '';
		var contentString = '';
		
		if(street !== '' && zipCode !== '' && city !== '') {
			address = street+', '+zipCode+' '+city;
			contentString = '<strong>'+title+'</strong><br/>'
				+street+'<br/>'
				+zipCode+' '+city;
			if(country !== '') {
				address = address+', '+country;
			}
		}
		
		var geocoder = new google.maps.Geocoder();
		console.log(geocoder);
		geocoder.geocode( {address:address}, function(results, status) 
		{
			if (status == google.maps.GeocoderStatus.OK) {
				if(status != google.maps.GeocoderStatus.ZERO_RESULTS) {
					var latitude = results[0].geometry.location.lat();
					var longitude = results[0].geometry.location.lng();
					njSetInputFieldValue(container,uid,'latitude',latitude);
					njSetInputFieldValue(container,uid,'longitude',longitude);
					njInitMap(container,uid,latitude,longitude,contentString);
				} else {
					alert("No results found");
				}
			} else {
				alert("Geocode was not successful for the following reason: " + status);
			}
		});
	};
	
	function njInitMap(container,uid,latitude,longitude,contentString) {
		$('#map'+uid).fadeIn(125);
		var latlng = new google.maps.LatLng(latitude, longitude);
	
		var myOptions = {
            zoom: 15,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: true
        };

        var map = new google.maps.Map(document.getElementById('map'+uid), myOptions);
		
		var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: njGetInputFieldValue(container,uid,'title')
        });
		if(contentString !== '') {
			var infowindow = new google.maps.InfoWindow({
				content: contentString
			});
			marker.addListener('click', function() {
				infowindow.open(map, marker);
			});
		}
	}
	
	function njGetInputFieldValue(container,uid,fieldName) {
		return container.find('input[name="data[tx_njbootstrap_domain_model_marker]['+uid+']['+fieldName+']"]').val();
	}
	function njSetInputFieldValue(container,uid,fieldName,value) {
		container.find('input[data-formengine-input-name="data[tx_njbootstrap_domain_model_marker]['+uid+']['+fieldName+']"]').val(value);
		container.find('input[name="data[tx_njbootstrap_domain_model_marker]['+uid+']['+fieldName+']"]').val(value);
	}
	
	$(document).ready(function() {
		var _animationSelector		= '[data-container="animationSelection"]';
		var _uid					= $(_animationSelector).attr('data-uid');
		var _animationInputSelector	= _animationSelector + ' select[data-type="animationSelection"]';
		var _animationImage			= '#animateCssTarget'+_uid;
	
		njAnimateCssInit(_uid,_animationInputSelector);
	
		$(_animationInputSelector).change(function(){
			var animationClass = $(this).val();
			$(_animationImage).animateCss(animationClass);
			njAnimateCssUpdateInput(_uid,animationClass);
		});
	
		$('[data-task="triggerAnimation"]').click(function(e){
			e.preventDefault();
			var animationClass = $(_animationInputSelector).val();
			$(_animationImage).animateCss(animationClass);
		});
		
		
		$('[data-task="getGeoData"]').click(function(e){
			e.preventDefault();
			var container = $(this).parents('.panel.panel-default.panel-condensed.panel-visible');
			var uid = $(this).attr('data-uid');
			njGetLocationData(container,uid)
		});
	});
});