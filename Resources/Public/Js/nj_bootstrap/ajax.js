/**
 * @author Nico Jatzkowski <nico@t3kk.de>
 * @package T3kk
 * @subpackage nj_bootstrap
 */

	
	var NJ_BOOTSTRAP_AJAX = function() {

		var bootstrap = new NJ_BOOTSTRAP();

		if (typeof(_nj_bootstrap_settings) !== "undefined") {
			this.ext_key = _nj_bootstrap_settings['ext_key'];
			this.typeNum = _nj_bootstrap_settings['typeNum'];
			this.language_uid = _nj_bootstrap_settings['language_uid'];
			this.pid = _nj_bootstrap_settings['pid'];
		} else {
			console.log('ERROR: You have to include var _nj_bootstrap, e.g. via typoscript or fluid.')
		}

		this._AJAX_ACTION_SUBMIT_FORMDATA = 'submit';

		this._DATATASK_AJAX_FILTER_JOBS = 'filterJobs';
		this._DATATASK_AJAX_FILTER_ARTICLES = 'filterArticles';
		this._DATATASK_AJAX_LOAD_MORE_JOBS = 'loadMoreJobs';
		this._DATATASK_AJAX_LOAD_MORE_ARTICLES = 'loadMoreNewsArticles';
		this._ATTR_DATA_ACTION_TYPE = 'data-action-type';
		this._ATTR_DATA_SELECTION_ALL = 'data-selection-all';
		this._ATTR_DATA_TASK = 'data-task';
		this._SELECTOR_AJAX_BUTTON = '.vd-button[data-type="ajax-button"]';
		this._SELECTOR_FILTER = '.vd-filter';
		this._SELECTOR_FILTER_SELECTION = '.selection';
		this._SELECTOR_FILTER_SELECT_MENU = '.vd-select-menu';
		this._SELECTOR_FILTER_SELECT_LIST = '[data-filter-type="select-list"]';
		this._SELECTOR_WALL	= 'ul.overview';
		this._STATUS_SELECTED = 'selected';
		this._STATUS_COLLAPSED = 'collapsed';
		this._STATUS_OPEN = 'open';


		/*this.addAjaxLoaderToButton = function(container) {
			container.prepend('<div class="icon ajaxLoader looper"><img src="typo3conf/ext/valid_edelman_ergo/ext_icon.svg" /></div>');

		};*/

		
		
		this.removeAjaxLoader = function(container) {
			container.children('.ajaxLoader').remove();
		};
		
		
		
		
	};


	


	NJ_BOOTSTRAP_AJAX.prototype.initialize = function(){};

	NJ_BOOTSTRAP_AJAX.prototype.defined = function($variable) {
		if($variable === undefined || $variable === 'undefined') {
			return false;
		} else {
			return true;
		}
	};


	//NJ_BOOTSTRAP_AJAX.prototype.getFormData = function(container) {
	//	console.log(container);
	//	alert(container.attr('data-ctype'));
	//};

	/**
	 * Ajax Handler
	 *
	 * @param params
	 * @param loader
	 */
	NJ_BOOTSTRAP_AJAX.prototype.ajaxHandler = function(params, loader) {

		var caller = this;

		if(params['controller'] == 'undefined') {
			params['controller']    = "Ajax";
		}
		
		var plugin = 'pi1';
		if(params['plugin'] != 'undefined') {
			plugin = params['plugin'];
		}
		
		var typeNum = this.typeNum[plugin];
		
		console.log(params);
		console.log(this);
		
		var data = 'index.php?'
			+ 'id=' + this.pid
			+ '&type=' + typeNum
			+ '&L=' + this.language_uid;

		console.log(this);

		for(var index in params) 
		{
			data = data.concat("&"+this.ext_key+"_"+plugin+"[",index,"]=",params[index]);
		}
		console.log(data);

		//vars used in beforeSend, success aso ...
		var listJobs;
		var listArticles;
		//

		$.ajax({
			async: 'true',
			url: data,
			type: 'POST',
			dataType: 'json',
			data: {
			},
			beforeSend: function() 
			{
				switch(params["action"]) 
				{
					case caller._AJAX_ACTION_SUBMIT_FORMDATA:
						$('.ce[data-uid="'+params['ce']+'"]').find('form').animate({
							maxWidth:0
						},250);
						$('.ce[data-uid="'+params['ce']+'"]').find('.nj-wrapper').hide(0);
						break;

					case caller._DATATASK_AJAX_FILTER_JOBS:

						//loader.addClass('ajax-loading');
						listJobs = loader.parents('[data-type="valid_jobs"]')
							.find('ul.vd-list');
						listJobs.slideUp(250);
						listJobs.find('li').remove();
						loader.parents('[data-type="valid_jobs"]').find('.vd-message').remove();
						break;

					case caller._DATATASK_AJAX_FILTER_ARTICLES:

						//loader.addClass('ajax-loading');

						listArticles = loader.parents('.content-box').find('.overview-wall > .articles-grid');

						var time = 125;
						var i = 0;

						listArticles.find('li').each(function() {
							setTimeout(function(){ $(this).remove(); }, time + (i * 25));
							$(this).fadeOut(time + (i * 25));
							i++;
						});

						loader.parents('[data-type="valid_newsroom"]').find('.vd-message').remove();
						loader.parents('[data-type="valid_newsroom"]').find('.vd-button').fadeOut(125);
						break;

					default:

						//nothing to do
					;    
				}

			},
			success: function(data) {
				switch(params["action"]) {
					case caller._AJAX_ACTION_SUBMIT_FORMDATA:
						if(data.success) {
							$('.ce[data-uid="'+params['ce']+'"]').find('.nj-wrapper').html(data.content).slideDown(250);
						}
						break;

					case caller._DATATASK_AJAX_LOAD_MORE_JOBS:
						listJobs = loader.parents('.content-box')
							.find('ul.vd-list');

						if(data.success) {
							listJobs.append(data.content);
							caller.postLoadedListItems(listJobs);
							listJobs.attr('data-items-offset',data.attributes.itemsOffset);
							if(!data.showButton) {
								loader.fadeOut();
							} else {
								loader.fadeIn();
							}
						} else {
							//TODO error-message(?)
						}
						break;

					case caller._DATATASK_AJAX_LOAD_MORE_ARTICLES:

						var list = loader.parents('.content-box').find('.overview-wall > .articles-grid');
						var filter = loader.parents('.content-box').find('[data-type="ajax-filter"] [data-filter-type="select-list"]');

						if(data.success) {
							list.find('[data-container="articles"]').append(data.content);
							caller.postLoadedArticles(loader);
							list.attr('data-items-offset',data.attributes.itemsOffset).attr('data-items-overall',data.attributes.itemsOverall);
							App.addPageSwipeListeners(list);

							if(!data.showButton) {
								loader.fadeOut();
							} else {
								loader.fadeIn().removeClass('inactive');
							}
						}	


						break;

					case caller._DATATASK_AJAX_FILTER_JOBS:
						if(data.success) {
							listJobs.html(data.content).slideDown(250);
							caller.postLoadedListItems(loader);
							loader.parents('ul').removeClass('inactive');

							listJobs
								.attr('data-items-offset',data.attributes.itemsLimit)
								.attr('data-items-limit',data.attributes.itemsLimit)
								.attr('data-items-overall',data.attributes.itemsOverall);

							if(data.showButton) {
								loader.parents('.content-box').find('.vd-button[data-type="ajax-button"]').fadeIn(125);
							} else {
								loader.parents('.content-box').find('.vd-button[data-type="ajax-button"]').fadeOut(125);
							}

						} else {
							loader.parents('.vd-filter').after(data.content);
						}
						break;

					case caller._DATATASK_AJAX_FILTER_ARTICLES:

						if(data.success) {
							listArticles
								.attr('data-items-overall',data.attributes.articlesOverall)
								.attr('data-items-offset',data.attributes.itemsOffset);
							listArticles.find('[data-container="articles"]')
								.html(data.content);
							caller.postLoadedArticles(loader);

							loader.parents(caller._SELECTOR_FILTER)
								.find(caller._SELECTOR_FILTER_SELECT_MENU+'-button[data-action-type="dropdown"]')
								.removeClass('inactive');

							loader.parents(caller._SELECTOR_FILTER)
								.find(caller._SELECTOR_FILTER_SELECT_MENU+'-button[data-action-type="dropdown"]')
								.removeClass('inactive');
							if(params['category'] !== '*') {
								loader.parents(caller._SELECTOR_FILTER)
									.find(caller._SELECTOR_FILTER_SELECT_MENU+'-button[data-action-type="button"]')
									.removeClass('inactive');
							}

							if(data.showButton) {
								loader.parents('.content-box').find('.vd-button').fadeIn(125);
							} else {
								loader.parents('.content-box').find('.vd-button').fadeOut(125);
							}

						} else {
							loader.parents('.vd-filter').after(data.content);
						}	

						break;
					default:
						//nothing to do
					;   
				}
			},
			error: function(xhr,e) 
			{	
				var message = '';
				if(xhr.status==0){
						message = 'You are offline!!\n Please Check Your Network.';
				}else if(xhr.status==404){
						message = 'Requested URL not found.\n' + xhr.responseText;
				}else if(xhr.status==500){
						message = 'Internel Server Error.\n' + xhr.responseText;
				}else if(e=='parsererror'){
						message = 'Parsing JSON Request failed.\n' + xhr.responseText;
				}else if(e=='timeout'){
						message = 'Request Time out.';
				}else {
						message = 'Unknown Error.\n' + xhr.responseText;
				}

				//if(params['controller'].valueOf() == 'Directory' && loader)
				//{
				//	var selectorResults = 'DIV.'+jqExtKey+'.'+params['controller'].toLowerCase()+'.resultList';
				//	$('body > DIV.ajaxLoader.std.large').remove();

				switch(params["action"]) 
				{
					case "uploadFile":
						$("FIELDSET[name=uploadData] .error_output").html(message);
						break;
					default:
						$('#validAjaxInfo').html(message);
				}
			}
		});

	};

	NJ_BOOTSTRAP_AJAX.prototype.postLoadedListItems = function(_this) {
		App.scrollTo(_this.parents('.content-box').find('.post-loaded').first());
		this.postLoadedItemsShow(_this.parents('.content-box').find('.post-loaded'),125,'fadeIn');
	}

	NJ_BOOTSTRAP_AJAX.prototype.postLoadedItemsShow = function(items,delay,effect) {
		var intTime = 0;
		items.each(function() {
			var _this = $(this)
			window.setTimeout(function(){
				_this.removeClass('post-loaded').animateCss(effect);
		  }, intTime);
		  intTime += delay;
		});
	}

	NJ_BOOTSTRAP_AJAX.prototype.postLoadedArticles = function(loader) {


		var postLoadedTeaser = loader.parents('.content-box').find('.post-loaded');

		App.scrollTo(postLoadedTeaser.first());

		var intTime = 0;
		var i = 1;
		postLoadedTeaser.each(function(){
		  var _this = this;
		  window.setTimeout(function(){
			  if(i===1) {
				  $(_this).removeClass('post-loaded').animateCss('fadeInTop');
			  } else {
				   $(_this).removeClass('post-loaded').animateCss('fadeIn');
			  }

		  }, intTime);
		  i++;
		  intTime += 350;
		});

	};