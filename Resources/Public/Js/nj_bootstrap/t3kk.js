(function (root, factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module.
		define(factory);
	} else if (typeof exports === 'object') {
		// CommonJS
		module.exports = factory();
	} else {
		// Browser global
		root.T3kk = factory();
	}
}(this, function () {
	"use strict";

	var T3kk = function () {
		//STATUS_PINNED : 'pinned'
//		
//		this._ACTION_GET_HEIGHT				= 'getHeight';
//		this._ACTION_GET_HEIGHT_DOCUMENT	= 'documentHeight';
//		this._ACTION_GET_HEIGHT_SCREEN		= 'screenHeight';
//		//this._ACTION_GET_HEIGHT_VIEWPORT	= 'vh';
//		this._ACTION_HIDE					= 'hide';
//		this._ACTION_INIT					= 'init';
//		this._ACTION_SHOW					= 'show';
//		this._ACTION_MAXIMIZE				= 'maximize';
//		this._ACTION_MINIMIZE				= 'minimize';
//		this._ACTION_GET_WIDTH				= 'getWidth';
//		this._ACTION_GET_WIDTH_DOCUMENT		= 'documentWidth';
//		this._ACTION_GET_WIDTH_SCREEN		= 'screenWidth';
//		this._ACTION_GET_WIDTH_SCROLLBAR	= 'scrollbarWidth';
//		//this._ACTION_GET_WIDTH_VIEWPORT		= 'vw';
//		
	};

	T3kk.ACTION_GET_VIEWVPORT_HEIGHT	= 'vh';
	T3kk.ACTION_GET_VIEWPORT_WIDTH		= 'vw';

	T3kk.STATUS_ACTIVE					= 'active';
	T3kk.STATUS_INACTIVE				= 'inactive';
	T3kk.STATUS_PINNED					= 'pinned';

	T3kk.CSS_EFFECT						= 'effect';

	/**
	 * @param {string} type
	 * @returns {number}
	 */
	T3kk.prototype.dimension = function(type) {	
		
		switch(type) {
			case this._ACTION_GET_HEIGHT_DOCUMENT:
				if (typeof document.body.offsetHeight !== 'undefined')
				{
					return document.body.offsetHeight;
				}
				break;
			case this._ACTION_GET_HEIGHT_SCREEN:
				if (typeof screen.availHeight !== 'undefined')
				{
					return  screen.availHeight;
				}
				break;
			case T3kk.ACTION_GET_VIEWVPORT_HEIGHT:
				if (typeof document.documentElement.clientHeight !== 'undefined')
				{
					return document.documentElement.clientHeight;
				}
				break;
			case this._ACTION_GET_WIDTH_DOCUMENT:
				if (typeof document.body.offsetWidth !== 'undefined')
				{
					return document.body.offsetWidth;
				}
				break;
			case this._ACTION_GET_WIDTH_SCREEN:
				if (typeof screen.availWidth !== 'undefined')
				{
					return screen.availWidth;
				}
				break;
			case this._ACTION_GET_WIDTH_SCROLLBAR:
				return scrollbar();
				break;
			case T3kk.ACTION_GET_VIEWPORT_WIDTH:
				if (typeof document.documentElement.clientWidth !== 'undefined')
				{
					return document.documentElement.clientWidth;
				}
				break;
			default:;
		} //end of switch
	
		function scrollbar() {
			var outer = document.createElement("div");
			outer.style.visibility = "hidden";
			outer.style.width = "100px";
			document.body.appendChild(outer);

			var widthNoScroll = outer.offsetWidth;
			// force scrollbars
			outer.style.overflow = "scroll";

			// add inner div
			var inner = document.createElement("div");
			inner.style.width = "100%";
			outer.appendChild(inner);        

			var widthWithScroll = inner.offsetWidth;

			// remove divs
			outer.parentNode.removeChild(outer);

			return widthNoScroll - widthWithScroll;
		}
	};


	
	// TODO: temporary workaround for chrome's scroll jitter bug
	window.addEventListener("mousewheel", function () {});


	$.fn.isOnScreen = function() {
		var win = $(window);

		var viewport = {
			top : win.scrollTop(),
			left : win.scrollLeft()
		};
		viewport.right = viewport.left + win.width();
		viewport.bottom = viewport.top + win.height();

		var bounds = this.offset();
		bounds.right = bounds.left + this.outerWidth();
		bounds.bottom = bounds.top + this.outerHeight();

		return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

	};
	
	$.fn.isScrolling = function() {
		var status = true;
		clearTimeout( $.data( this, "scrollCheck" ) );
		$.data( this, "scrollCheck", setTimeout(function() {
			status = false;
			return status;
		}, 250) );
	};
	
	
	$.fn.scrolla = function(func,detectEndOfScrolling=false) {
		$(this).scroll(function() {
			
			function execute(func) {
				if(func != undefined && typeof func == 'function') {
					func();
				}
			};
			
			if(detectEndOfScrolling) {
				clearTimeout( $.data( this, "scrollCheck" ) );
				$.data( this, "scrollCheck", setTimeout(function() {
					execute(func);
				}, 250) );
			}
			else {
				execute(func);
			}
		});
	};
	

	return T3kk;
}));