/**
 * @author Nico Jatzkowski <nico@t3kk.de>
 * @package T3kk
 * @subpackage nj_bootstrap
 */

var NJ_BOOTSTRAP_FORM = function() {
	
	var bootstrap = new NJ_BOOTSTRAP();
	
	this.changeSubmitButtonStatus = function(trigger,conIdentifier) {
//		var checkedOverall = 0;
//		trigger.parents(conIdentifier).find('[data-check-status="1"]').each(function() {
//			var checked = 0;
//			if($(this).find('input[type="radio"],input[type="checkbox"]').length > 0) {
//				$(this).find('input[type="radio"],input[type="checkbox"]').each(function() {
//					if($(this).is(':checked')) { checked=checked+1; }
//				});
//			}
//			if(checked > 0) {
//				checkedOverall=checkedOverall+1;
//			}
//		});

	};
	
	
	this.checkFormField = function(field) {
		var status = 1;
		if(field.attr('data-required') === '1') {
			if(field.is('input[type="text"],input[type="email"]')) {
				if(!field.val()) {
					status = 0;
				}
			}
			if(field.is('textarea')) {
				if(!$.trim(field.val())) {
					status = 0;
				}
			}
		}
		if(field.is('input[type="email"]')) {
			if(field.val() && !this.validateEmail(field.val())) {
				status = 0;
			}
		}
		field.attr('data-valid',status);
		
		var disabled = false;
		if(field.parents('.nj-form').find('[data-valid="0"]').length > 0) {
			disabled = true;
		}
		field.parents('.nj-form')
			.find('.nj-button.nj-button--form').prop("disabled", disabled);
	};
	
	this.checkFormData = function(container) {
		var errors = [];
		container.find('input[type="text"],input[type="email"],input[type="url"]').each(function() {
			if($(this).is('input') && $(this).val()) {
				switch($(this).attr('type')) {
					case 'email':
						if(!this.validateEmail($(this).val())) {
							errors[$(this).attr('id')] = 'email';
						}
						break;
					default:;
				}
			}
			if($(this).attr('data-required') === "1" && !$(this).val()) {
				errors[$(this).attr('id')] = 'required';
			}
		});
		if(!bootstrap.empty(errors)) { return errors; }
		return false;
	};
	
	this.getFormData = function(container) {
		var formData = [];
		
		container.find('input[type="text"],input[type="email"],input[type="url"],textarea').each(function() {
			formData[$(this).attr('id')] = $(this).val();
		});
		return formData;
	};
	
	this.validateEmail = function(email) {
		var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return regex.test(email);
	};
	
};