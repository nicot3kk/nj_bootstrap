<?php
if(!defined('TYPO3_MODE')) die ('Access denied.');
 
$extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY);

/**
 * Registers a Plugin to be listed in the Backend. You also have to configure the Dispatcher in ext_localconf.php.
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    $_EXTKEY,
    'Pi1',
    'njs Bootstrap package'
);

/**
 * Registers a Plugin to be listed in the Backend. You also have to configure the Dispatcher in ext_localconf.php.
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    $_EXTKEY,
    'Contact',
    'njs Bootstrap package -> Contact'
);


 
/**
 * TypoScript
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'njs Bootstrap package setup');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript/Print', 'njs Bootstrap package print');

/**
 * Flexform
 */
$plugins = ['pi1','contact'];

// Clean up the Flexform fields in the backend a bit
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][strtolower($extensionName)] = 'layout,select_key,splash_layout';

foreach($plugins as $plugin) {
	$pluginSignature = strtolower($extensionName) . '_' . $plugin;
	// Add own flexform stuff.
	$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_'.$pluginSignature.'.xml');
}





//call_user_func(
//    function ($extKey,$table) {
	
		/**
 * Set available fields for extra page types
 */
//	\T3kk\NjBootstrap\Service\GeneralService::registerIcons(
//		$table
//	);
		
//	},
//    'nj_bootstrap',
//	\T3kk\NjBootstrap\Service\GeneralService::TABLE_PAGES
//);


// Allow backend users to drag and drop the new page type:
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
//            'options.pageTree.doktypesToShowInNewPageDragArea := addToList(251)'
//        );



call_user_func(
	function () {
		$tables = [
			0 => 'tx_njbootstrap_domain_model_icon',
			1 => 'tx_njbootstrap_domain_model_marker',
			2 => 'tx_njbootstrap_domain_model_slide'
		];
		
		foreach($tables as $table) {
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages($table);
		}
	}
);


/**
 * Register custom doktypes
 */
call_user_func(
    function ($extKey,$table,$langFile) {
	
		$customDoktypes = [
			250 => [
				'name' => 'data',
				'allowedTables' => '*',
				'icon' => [
					'filename' => 'doktype_data',
					'identifier' => 'njbootstrap-pages-doktype-data'
				],
				'type' => 'sys',
			]
		];
		
		foreach($customDoktypes as $doktype => $value) {
			// Add new page type
			$GLOBALS['PAGES_TYPES'][$doktype] = [
				'type' => $value['type'],
				'allowedTables' => '*',
			];
			
			// Add the new doktype to the page type selector
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
				$table,
				'doktype',
				[
					$langFile.'doktype.' .$value['name'],
					$doktype,
					'EXT:' . $extKey . '/Resources/Public/Icons/'.$value['icon']['filename'].'.svg'
				],
				'1',
				'after'
			);
			
			
			$states= ['','-hideinmenu','-domain'];
			foreach($states as $status) {
				$GLOBALS['TCA'][$table]['ctrl']['typeicon_classes'][$doktype.$status] = $value['icon']['identifier'].$status;
			
				\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class)
					->registerIcon(
						$value['icon']['identifier'].$status,
						\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
						['source' => 'EXT:' . $extKey . '/Resources/Public/Icons/'.$value['icon']['filename'].$status.'.svg']
				);
			}
			
			// Allow backend users to drag and drop the new page type:
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
				'options.pageTree.doktypesToShowInNewPageDragArea := addToList(' . $doktype . ')'
			);
		}
	},
	'nj_bootstrap',
	'pages',
	'LLL:EXT:nj_bootstrap/Resources/Private/Language/locallang_be.xlf:'
);
	
call_user_func(
    function ($namespace,$ext,$table,$langFile) {
	
	//$tcaType = 'nj_'.$identifier;
	//$iconIdentifier = self::EXT_KEY_SHORT.'-ttcontent-'.$identifier;
	
		$contentElements = [
			'nj_carousel' => [
				'svg' => 'ce_carousel',
				'identifier' => 'carousel'
			],
			'nj_form' => [
				'svg' => 'ce_form',
				'identifier' => 'form'
			],
			'nj_hero' => [
				'svg' => 'ce_hero',
				'identifier' => 'hero'
			],
			'nj_icons' => [
				'svg' => 'ce_icons',
				'identifier' => 'icons'
			],
			'nj_maps' => [
				'svg' => 'ce_maps',
				'identifier' => 'maps'
			],
			'nj_text' => [
				'svg' => 'ce_text',
				'identifier' => 'text'
			],
			'nj_textimage' => [
				'svg' => 'ce_textimage',
				'identifier' => 'textimage'
			],
		];
		
		foreach($contentElements as $ce => $value) {
		
			\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class)
				->registerIcon(
					$namespace.$ext.'-ttcontent-'.$value['identifier'],
					\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
					['source' => 'EXT:' . $namespace . '_' .$ext. '/Resources/Public/Icons/'.$value['svg'].'.svg']
				);
			
			$GLOBALS['TCA'][$table]['ctrl']['typeicon_classes'][$ce] = $extKeyShort.'-ttcontent-'.$value['identifier'];
			
			\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
				[
					$langFile.'ce.'.$value['identifier'],
					$ce,
					#'EXT:nj_bootstrap/Resources/Public/Icons/ce_carousel.svg'
					$namespace.$ext.'-ttcontent-'.$value['identifier']
				],
			   'CType',
			   $ce
			);
		}
	},
	'nj',
	'bootstrap',
	'tt_content',
	'LLL:EXT:nj_bootstrap/Resources/Private/Language/locallang_be.xlf:'
);