<?php
/***************************************************************
 * Extension Manager/Repository config file for ext: "nj_page"
***************************************************************/
 
$EM_CONF[$_EXTKEY] = array(
	'title' => 'njs TYPO3 Bootstrap package',
	'description' => 'Page configuration & assets for websites and extensions developed by Nico J.',
	'category' => 'plugin',
	'author' => 'Nico Jatzkowski',
	'author_email' => 'nico@t3kk.de',
	'author_company' => 't3kk.de',
	'shy' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'version' => '8.5.0',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
			'extbase' => '8.4.0-0.0.0',
			'fluid' => '8.4.0-0.0.0',
			'typo3' => '8.4.0-0.0.0',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => '',
	'autoload' => [
		'psr-4' => ['T3kk\\NjBootstrap\\' => 'Classes']
	],
);