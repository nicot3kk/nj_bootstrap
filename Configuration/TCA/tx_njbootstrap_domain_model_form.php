<?php
if (!defined('TYPO3_MODE')) { die('Access denied.'); }

$model = 'form';

$ext = 'tx_njbootstrap';
$extKey = 'nj_bootstrap';
$langFile = 'LLL:EXT:nj_bootstrap/Resources/Private/Language/locallang_be.xlf:';


/**
 * @param string $label
 * @return array
 */
function fieldInput($label,$max,$size=40) {
	return [
		'label' => $langFile.$label,
		'exclude' => 1,
		'config' => [
			'type' => 'input',
			'size' => $size,
			'max' => $max,
			'eval' => 'trim',
		]
	];
}

return [
	'ctrl' => [
		'crdate' => 'crdate',
		'default_sortby' => 'ORDER BY crdate ASC',
		'delete' => 'deleted',
		'dividers2tabs' => TRUE,
		'enablecolumns' => [
			'disabled' => 'hidden'
		],
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey) . 'Resources/Public/Icons/' . $ext . '_domain_model_' . $model . '.svg',
		'label' => 'title',
		//'sortby' => 'sorting',
		'title' => $langFile.'model.'.$model,
		'tstamp' => 'tstamp',
	],
	'interface' => [
		'showRecordFieldList' => 'hidden,'
	],
	'columns' => [
		'country' => fieldInput('label.country',25),
		'email' => fieldInput('label.email',255),
		'firm' => fieldInput('label.firm',50),
		'firstName' => fieldInput('label.firstName',25),
		'lastName' => fieldInput('label.lastName',25),
		'location' => fieldInput('label.location',25),
		'message' => [
			'exclude' => 1,
			'label'   => $langFile.'label.text',
			'config'  => [
				'type' => 'text',
				'cols' => 60,
				'max' => 256,
				'rows' => 6,
			]
		],
		'mobile' => fieldInput('label.mobile',25),
		'name' => fieldInput('label.name',50),
		'phone' => fieldInput('label.phone',25),
		'street' => fieldInput('label.street',50),
		'subject' => fieldInput('label.subject',125),
		'zipCode' => fieldInput('label.zipCode',10,5),
		'url' => fieldInput('label.url',255),
		
		'log_ip' => fieldInput('label.logIp',25,10),
	],
	'types' => [
		'1' => [
			'showitem' => 'hidden,subject,email,message,firm,name,firstName,lastName,street,zipCode,location,country,phone,mobile,url,log_ip',
		],
	],
	'palettes' => [
		'1' => [
			'showitem' => '',
		],
	]
];