<?php

$extKey		= 'nj_bootstrap';
$table	= 'tt_content';
$extLang	= 'LLL:EXT:nj_bootstrap/Resources/Private/Language/locallang_be.xlf:';

$defaultContentFields = [
	'default' => 'nj_render_type,nj_render_version',
	'rendering' => ',--div--;'.$extLang.'tab.rendering,nj_render_type,nj_render_version',
	'style' => ',--div--;'.$extLang.'tab.design,'
		. 'nj_alignment,nj_color_sheme,nj_css_classes,nj_css_inline',
	'button' => ',--div--;'.$extLang.'tab.button,' 
		. 'nj_btn_use,nj_btn_label,nj_btn_type,nj_btn_class,nj,nj_btn_link,nj_btn_action,nj_btn_action_data,nj_btn_is_ajax'
];



/**
 * render definitions
 */
call_user_func(
	function ($extKey,$table,$extLang) {
		$fields = [
			'nj_render_type' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude' => 0,
				'label'   => $extLang.'tca.renderType',
				'config' => [
					'type' => 'select',
					'renderType' => 'selectSingle',
					'items' => [
						[$extLang.'select.general.default',''],
						['article', 'article'],
						['div', 'div'],
						['section', 'section'],
					],
					'default' => '',
					'maxitems' => 1
				]
			],
			'nj_render_version' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude' => 0,
				'label'   => $extLang.'tca.renderVersion',
				'config' => [
					'type' => 'select',
					'renderType' => 'selectSingle',
					'items' => [
						[$extLang.'select.general.default',''],
						['Hidden', 1],
						['Flesh Message', 2],
						['Ajax', 3],
					],
					'default' => 1,
					'maxitems' => 1
				]
			],
		];
		
		addTCAcolumns($fields);
	}
	,$extKey,$table,$extLang
);



/**
 * default fields:
 * deleted,hidden,
 * header,subheader,bodytext,header_layout,layout,menu_type,sys_language_uid
 */

call_user_func(
	function ($extKey,$table,$extLang) {
	
		$tcaAddFieldsDefault = [
			'nj_animation' => [
				'displayCond' => [
					'AND' => [
						'FIELD:sys_language_uid:<=:0',
						'FIELD:nj_animation_enable:>:0'
					]
				],
				'exclude' => 1,
				'label'   => $extLang.'tca.animateCss.chosen',
				'config'  => [
					'type' => 'input',
					'size' => 25,
					'eval' => 'trim',
					'max'  => 256,
				]
			],
			'nj_animation_enable' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'onChange' => 'reload',
				'exclude' => 1,
				'label'   => $extLang.'tca.animateCss.activate',
				'config' => [
					'type' => 'check'
				]

			],
			'nj_animation_selection' => [
				'displayCond' => [
					'AND' => [
						'FIELD:sys_language_uid:<=:0',
						'FIELD:nj_animation_enable:>:0'
					]
				],
				'exclude' => 1,
				'label' => '',
				'config' => [
					'type' => 'user',
					'userFunc' => 'T3kk\NjBootstrap\Utility\TcaUtility->animationSelection',
					'parameters' => []
				]
			],
			'nj_bg_image' => [
				'label' => 'Hintergrund-Bild',
				'l10n_mode' => 'mergeIfNotBlank',
				'exclude' => 1,
				'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
					'appearance' => array(
						'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference',
					),
					'foreign_match_fields' => array(
					   'fieldname' => 'nj_bg_image',
					),
					'minitems' => 0,
					'maxitems' => 1,
				],
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
			],
			'nj_image' => [
				'label' => 'Bild',
				'l10n_mode' => 'mergeIfNotBlank',
				'exclude' => 1,
				'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
					'appearance' => array(
						'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference',
					),
					'foreign_match_fields' => array(
					   'fieldname' => 'nj_image',
					),
					'minitems' => 0,
					'maxitems' => 1,
				],
				$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
			],
			'nj_image_filter_enable' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude'	=> 1,
				'label'		=> 'Activate image filter',
				'config'  => [
					'type' => 'check'
				],
				'default' => 0
			],
			'nj_image_filter' => [
				'displayCond' => [
					'AND' => [
						'FIELD:sys_language_uid:<=:0',
						'FIELD:nj_image_filter_enable:=:1',
					]
				],
				'exclude'	=> 1,
				'label'		=> 'Image filter',
				'config'  => [
					'type' => 'select',
					'renderType' => 'selectSingle',
					'items' => [
						['1977','1977'],
						['Aden','aden'],
						['Amaro','amaro'],
						['Brannan','brannan'],
						['Brooklyn','brooklyn'],
						['Darken *','darken'],
						['Earlybird','earlybird'],
						['Gingham','gingham'],
						['Hefe','hefe'],
						['Hudson','hudson'],
						['Inkwell','inkwell'],
						['Kelvin','kelvin'],
						['Lo-Fi','lofi'],
						['Mayfair','mayfair'],
						['Nashville','nashville'],
						['Perpetua','perpetua'],
						['Reyes','reyes'],
						['Rise','rise'],
						['Sierra','sierra'],
						['Sutro','sutro'],
						['Toaster','toaster'],
						['Valencia','valencia'],
						['Walden','walden'],
						['Willow','willow'],
						['X-PRO 2','xpro2'],
					],
				]
			],
			'nj_load_assets' => [
				'exclude' => 0,
				'label' => '',
				'config' => [
					'type' => 'user',
					'userFunc' => 'T3kk\NjBootstrap\Utility\TcaUtility->loadAssets',
					'parameters' => []
				]
			],
			'nj_menu_display' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude'	=> 1,
				'label'		=> 'Im Menu anzeigen',
				'config'  => [
					'type' => 'check'
				],
				'default' => 0
			],
			'nj_menu_title' => [
				'displayCond' => 'FIELD:nj_menu_display:>:0',
				'exclude' => 1,
				'label'   => 'Menu-Titel',
				'config'  => [
					'type' => 'input',
					'size' => 25,
					'eval' => 'trim',
					'max'  => 25,
				]
			],
			'nj_singular_content' => [
				'exclude' => 1,
				'label'   => 'Einmaligen Content verwenden',
				'config'  => [
					'type' => 'check'
				],
				'default' => 0
			],
			'nj_version' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude'	=> 1,
				'label'		=> 'Version',
				'config'  => [
					'type' => 'select',
					'renderType' => 'selectSingle',
					'items' => [
						['Default',1],
					],
				]
			],
		];
		
		addFieldsToPalette(
			'nj_animation',
			'nj_animation_enable,--linebreak--,nj_animation_selection,--linebreak--,nj_animation'
		);
		
		addTCAcolumns($tcaAddFieldsDefault);
		extendRequestUpdateFields('nj_animation_enable,nj_image_filter_enable,nj_menu_display,');
	},
	$extKey,$table,$extLang
);

	
call_user_func(
	function($extKey,$table,$extLang) {
	
		$columns = [
			'nj_btn_action' => [
				'displayCond' => [
					'AND' => [
						'FIELD:sys_language_uid:<=:0',
						'FIELD:nj_btn_use:=:1',
						'FIELD:nj_btn_type:=:2',
					]
				],
				'exclude' => 1,
				'label'   => 'Aktion',
				'config'  => [
					'type' => 'input',
					'size' => 40,
					'eval' => 'trim',
					'max'  => 50,
				]
			],
			'nj_btn_action_data' => [
				'displayCond' => [
					'AND' => [
						'FIELD:sys_language_uid:<=:0',
						'FIELD:nj_btn_use:=:1',
						'FIELD:nj_btn_type:=:2',
					]
				],
				'exclude' => 1,
				'label'   => 'Aktion -> Data-Attributes',
				'config'  => [
					'type' => 'input',
					'size' => 40,
					'eval' => 'trim',
					'max'  => 255,
				]
			],
			'nj_btn_class' => [
				'displayCond' => [
					'AND' => [
						'FIELD:sys_language_uid:<=:0',
						'FIELD:nj_btn_use:=:1',
					]
				],
				'exclude' => 1,
				'label'   => 'Button-Klasse (testklasse wird als nj-button--testklasse ausgegeben)',
				'config'  => [
					'type' => 'input',
					'size' => 40,
					'eval' => 'trim',
					'max'  => 255,
				]
			],
			'nj_btn_is_ajax' => [
				'displayCond' => [
					'AND' => [
						'FIELD:sys_language_uid:<=:0',
						'FIELD:nj_btn_use:=:1',
						'FIELD:nj_btn_type:=:2',
					]
				],
				'exclude' => 1,
				'label'   => 'Ajax',
				'config'  => [
					'type' => 'check'
				],
				'default' => 0
			],
			'nj_btn_label' => [
				'displayCond' => 'FIELD:nj_btn_use:=:1',
				'exclude' => 1,
				'label'   => 'Label',
				'config'  => [
					'type' => 'input',
					'size' => 40,
					'eval' => 'trim',
					'max'  => 50,
				]
			],
			'nj_btn_link' => [
				'displayCond' => [
					'AND' => [
						'FIELD:sys_language_uid:<=:0',
						'FIELD:nj_btn_use:=:1',
						'FIELD:nj_btn_type:<:2',
					]
				],
				'exclude' => 1,
				'label' => 'LLL:EXT:example/locallang_db.xml:tt_content.link',
				'config' => [
					'type' => 'input',
					'size' => 40,
					'softref' => 'typolink',
					'wizards' => array(
						'_PADDING' => 2,
						'link' => array(
							 'type' => 'popup',
							 'title' => 'Link',
							 'icon' => 'EXT:example/Resources/Public/Images/FormFieldWizard/wizard_link.gif',
							 'module' => array(
								'name' => 'wizard_element_browser',
								'urlParameters' => array(
									'mode' => 'wizard'
								) ,
							 ) ,
							 'JSopenParams' => 'height=600,width=500,status=0,menubar=0,scrollbars=1'
						)
					)
				]
			],
			'nj_btn_type' => [
				'displayCond' => [
					'AND' => [
						'FIELD:sys_language_uid:<=:0',
						'FIELD:nj_btn_use:=:1',
					]
				],
				'exclude'	=> 1,
				'label'		=> 'Typ',
				'config'  => [
					'type' => 'select',
					'renderType' => 'selectSingle',
					'items' => [
						['Link','0'],
						['Email','1'],
						['Javascript','2'],
					],
				]
			],
			'nj_btn_use' => [
				'exclude' => 1,
				'label'   => 'Button verwenden',
				'config'  => [
					'type' => 'check'
				],
				'default' => 0
			],
		];
		extendRequestUpdateFields('nj_btn_use,nj_btn_type');
		addTCAcolumns($columns);
	},
	$extKey,$table,$extLang
);
	
/**
 * style fields:
 * -------------------
 * deleted,hidden,
 * header,subheader,bodytext,header_layout,layout,menu_type,sys_language_uid
 */
call_user_func(
	function($extKey,$table,$extLang) {
		$columnsStyle = [
			'nj_alignment' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude' => 0,
				'label' => $extLang.'tca.alignment',
				'config' => [
					'type' => 'select',
					'renderType' => 'selectSingle',
					'items' => [
							[$extLang.'select.general.default', ''],
							[$extLang.'tca.alignment.left', 'left'],
							[$extLang.'tca.alignment.center', 'center'],
							[$extLang.'tca.alignment.right', 'right'],
							[$extLang.'tca.alignment.justify', 'justify'],
					],
					'default' => '',
					'maxitems' => 1
				]
			],
			'nj_css_classes' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude' => 0,
				'label' => $extLang.'tca.cssClasses',
				'config'  => [
					'type' => 'input',
					'size' => 25,
					'eval' => 'trim',
					'max'  => 256
				]
			],
			'nj_css_inline' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude' => 0,
				'label'   => $extLang.'tca.cssInline',
				'config'  => [
					'type' => 'input',
					'size' => 25,
					'eval' => 'trim',
					'max'  => 256
				]
			],
			'nj_color_sheme' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude' => 0,
				'label' => $extLang.'tca.colorSheme',
				'config' => [
					'type' => 'select',
					'renderType' => 'selectSingle',
					'items' => [
							[$extLang.'select.general.default', ''],
							[$extLang.'tca.colorSheme.1', 'colorsheme-1'],
							[$extLang.'tca.colorSheme.2', 'colorsheme-2'],
							[$extLang.'tca.colorSheme.3', 'colorsheme-3'],
					],
					'default' => '',
					'maxitems' => 1
				]
			],
		];
	
		\T3kk\NjBootstrap\Utility\TcaUtility::addTCAcolumns($table, $columnsStyle);
		
	},
	$extKey,$table,$extLang
);
	
	

call_user_func(
	function ($extKey,$table,$extLang) {
		$tcaAddFieldsInfoTexts = [
			'nj_info_random_image' => tcaFieldInfoText('tca.info.randomImage',$extLang),
		];
		
		addTCAcolumns($tcaAddFieldsInfoTexts);
	},
	$extKey,$table,$extLang
);
	

/**
 * Content element : nj_carousel
 */
call_user_func(
    function ($extKey,$table,$extLang) {
		
		$tcaAddFieldsSlider = [
			'nj_autoplay' => [
				'exclude' => 1,
				'label'   => 'Autoplay',
				'config'  => [
					'type' => 'check'
				],
				'default' => 1
			],
			'nj_direction_prev' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude'	=> 1,
				'label'		=> 'Direction next',
				'config'  => [
					'type' => 'select',
					'renderType' => 'selectSingle',
					'items' => [
						['toLeft','toLeft'],
						['toRight','toRight'],
						['toTop','toTop'],
						['toBottom','toBottom'],
					],
				]
			],
			'nj_direction_next' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude'	=> 1,
				'label'		=> 'Direction next',
				'config'  => [
					'type' => 'select',
					'renderType' => 'selectSingle',
					'items' => [
						['toLeft','toLeft'],
						['toRight','toRight'],
						['toTop','toTop'],
						['toBottom','toBottom'],
					],
				]
			],
			'nj_easing' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude'	=> 1,
				'label'		=> 'Easing',
				'config'  => [
					'type' => 'select',
					'renderType' => 'selectSingle',
					'items' => [
						['jswing','jswing'],
						['def','def'],
						['easeInBack','easeInBack'],
						['easeInBounce','easeInBounce'],
						['easeInCirc','easeInCirc'],
						['easeInCubic','easeInCubic'],
						['easeInElastic','easeInElastic'],
						['easeInExpo','easeInExpo'],
						['easeInQuad','easeInQuad'],
						['easeInQuart','easeInQuart'],
						['easeInQuint','easeInQuint'],
						['easeInSine','easeInSine'],
						['easeOutBack','easeOutBack'],
						['easeOutBounce','easeOutBounce'],
						['easeOutCirc','easeOutCirc'],
						['easeOutCubic','easeOutCubic'],
						['easeOutElastic','easeOutElastic'],
						['easeOutExpo','easeOutExpo'],
						['easeOutQuad','easeOutQuad'],
						['easeOutQuart','easeOutQuart'],
						['easeOutQuint','easeOutQuint'],
						['easeOutSine','easeOutSine'],
						['easeInOutBack','easeInOutBack'],
						['easeInOutBounce','easeInOutBounce'],
						['easeInOutCirc','easeInOutCirc'],
						['easeInOutCubic','easeInOutCubic'],
						['easeInOutElastic','easeInOutElastic'],
						['easeInOutExpo','easeInOutExpo'],
						['easeInOutQuad','easeInOutQuad'],
						['easeInOutQuart','easeInOutQuart'],
						['easeInOutQuint','easeInOutQuint'],
						['easeInOutSine','easeInOutSine'],
					],
				]
			],
			'nj_effect' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude'	=> 1,
				'label'		=> 'Effect',
				'config'  => [
					'type' => 'select',
					'renderType' => 'selectSingle',
					'items' => [
						['none','none'],
						['chewyBars','chewyBars'],
						['explode','explode'],
						['fade','fade'],
						['kick','kick'],
						['shuffle','shuffle'],
						['slide','slide'],
						['transfer','transfer'],
						['turnOver','turnOver'],
					],
				],
			],
			'nj_pause_on_hover' => [
				'exclude' => 1,
				'label'   => 'Paise on hover',
				'config'  => [
					'type' => 'check'
				],
				'default' => 1
			],
			'nj_slides' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude'	=> 1,
				'label'		=> 'Slides',
				'config' => [
					'type' => 'inline',
					'foreign_table' => 'tx_njbootstrap_domain_model_slide',
					'foreign_table_field' => 'foreign_table',
					'foreign_field' => 'foreign_uid',
					'foreign_sortby' => 'foreign_sorting',
					'foreign_record_defaults' => [
						'foreign_field' => 'nj_slides'
					 ],
					'foreign_match_fields' => [
						'foreign_field' => 'nj_slides'
					],
					'maxitems' => 99,
					'appearance' => [
						'collapseAll' => 0,
						'levelLinksPosition' => 'top',
						'showSynchronizationLink' => 1,
						'showPossibleLocalizationRecords' => 1,
						'showAllLocalizationLink' => 1
					],
				],
			],
			'nj_time_effect' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude' => 1,
				'label'   => 'Animation duration',
				'config'  => [
					'type' => 'input',
					'size' => 10,
					'eval' => 'int,trim',
					'max'  => 6
				],
			],
			'nj_time_show' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude' => 1,
				'label'   => 'Time between animation',
				'config'  => [
					'type' => 'input',
					'size' => 10,
					'eval' => 'int,trim',
					'max'  => 6
				],
			],
		];
		
		addTCAcolumns($tcaAddFieldsSlider);
		
		
		/**
		 * Add own content elements
		 */
		$njCarousel = [
			'types' => [
				'nj_carousel' => [
					'showitem' => '--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,'
						.'header,subheader,nj_menu_display,nj_menu_title,bodytext,'
						.'--div--;'.$extLang.'tab.carouselOptions,'
						.'nj_singular_content,'
						.'nj_version,'
						.'nj_effect,nj_easing,nj_time_show,nj_time_effect,nj_autoplay,nj_pause_on_hover,nj_direction_next,nj_direction_prev,'
						.'--div--;'.$extLang.'tab.slides,'
						.'nj_slides,'
				]
			],
			'columns' => array(
				'CType' => array(
					'config' => array(
						'items' => array(
							'nj_carousel' => array(
								'Carousel', // Name des Inhaltselementes
								'nj_carousel', // TCA Name des Inhaltselementes'
								'EXT:nj_bootstrap/Resources/Public/Icons/ce_carousel.svg' // Bild des Inhaltelementes
							)
						)
					)
				),
			)
		];
		
		addContentElement($njCarousel);
		
		/**
		 * Columns overrides
		 */
		
		$GLOBALS['TCA']['tt_content']['types']['nj_carousel']['columnsOverrides'] = [
			'nj_version' => [
				'config'  => [
					'items' => [
						['Bootstrap','bootstrap'],
						['FlexSlider','flexSlider'],
						#http://www.rhinoslider.com/options/
						['RhinoSlider','rhinoSlider'],
						['Slick','slick'],
						['Slippry','slippry'],
					],
				],
			],
		];
				
	},
	$extKey,$table,$extLang
);	
	
include_once 'ce/nj_icons.php';
	
/**
 * Content element : nj_maps
 */
call_user_func(
    function ($extKey,$table,$extLang) {
	
		$tcaAddFieldsGooglemaps = [
			'nj_gm_api_key' => [
				'exclude' => 1,
				'label'   => 'Google API key',
				'config'  => [
					'type' => 'input',
					'size' => 40,
					'eval' => 'trim',
					'max'  => 50,
				]
			],
			'nj_gm_marker' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude'	=> 1,
				'label'		=> 'Google marker',
				'config' => [
					'type' => 'inline',
					'foreign_table' => 'tx_njbootstrap_domain_model_marker',
					'foreign_table_field' => 'foreign_table',
					'foreign_field' => 'foreign_uid',
					'foreign_sortby' => 'foreign_sorting',
					'foreign_record_defaults' => [
						'foreign_field' => 'nj_gm_marker'
					 ],
					'foreign_match_fields' => [
						'foreign_field' => 'nj_gm_marker'
					],
					'maxitems' => 999,
					'appearance' => [
						'collapseAll' => 0,
						'enabledControls' => [
							'info' => true,
							'new' => false,
							'dragdrop' => true,
							'sort' => true,
							'hide' => true,
							'delete' => true,
							'localize' => false,
						],
						'levelLinksPosition' => 'top',
						'showSynchronizationLink' => 1,
						'showPossibleLocalizationRecords' => 1,
						'showAllLocalizationLink' => 1
					],
				],
			],
		];
		
		addTCAcolumns($tcaAddFieldsGooglemaps);
		
	
		/**
		 * Add own content elements
		 */
		$njMaps = [
			'types' => [
				'nj_maps' => [
					'showitem' => '--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,'
						. 'header,subheader,nj_menu_display,nj_menu_title,'
						. '--div--;'.$extLang.'tab.mapsOptions,'
						. 'nj_load_assets,nj_gm_api_key,nj_gm_marker,'
				]
			],
			'columns' => array(
				'CType' => array(
					'config' => array(
						'items' => array(
							'nj_maps' => array(
								'Google maps', // Name des Inhaltselementes
								'nj_maps', // TCA Name des Inhaltselementes'
								'EXT:nj_bootstrap/Resources/Public/Icons/ce_maps.svg' // Bild des Inhaltelementes
							)
						)
					)
				),
			)
		];
		
		addContentElement($njMaps);
	
	},
	$extKey,$table,$extLang
);	
	
//include_once 'ce/nj_form.php';
include_once 'ce/nj_hero.php';
include_once 'ce/nj_maps.php';
include_once 'ce/nj_text.php';
include_once 'ce/nj_textimage.php';


function addContentElement($ce) {
	$GLOBALS['TCA']['tt_content'] = array_replace_recursive($GLOBALS['TCA']['tt_content'], $ce);
}	


function addFieldsToPalette($palette,$fields) {
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
		'tt_content',
		$palette,
		$fields
	);
}

function addTCAcolumns($tcaColumns) {
	if(!empty($tcaColumns)) {
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
			'tt_content',
			$tcaColumns
		);
	}
}	


function extendRequestUpdateFields($requestUpdateFields) {
	$GLOBALS['TCA']['tt_content']['ctrl']['requestUpdate'] = $GLOBALS['TCA']['tt_content']['ctrl']['requestUpdate'] ? $GLOBALS['TCA']['tt_content']['ctrl']['requestUpdate'] . ',' . $requestUpdateFields : $requestUpdateFields;
}	
	
function tcaFieldInfoText($label,$extLang) {
	return [
		'exclude' => 1,
		'config' => [
			'type' => 'user',
			'userFunc' => 'T3kk\NjBootstrap\Utility\TcaUtility->infoText',
			'parameters' => [
				'text' => $extLang.$label
			],
		],
	];
}
	

function addCtypeToColumns($ce) {
	if(is_string($ce)) {
		return [
			'config' => [
				'items' => [
					'nj_'.$ce => [
						ucfirst($ce), // Name des Inhaltselementes
						'nj_'.$ce, // TCA Name des Inhaltselementes'
						'EXT:nj_bootstrap/Resources/Public/Icons/ce_'.$ce.'.svg' // Bild des Inhaltelementes
					]
				]
			]
		];
	}
}


	/**
	 * // Configure the default backend fields for the content element
		$GLOBALS['TCA']['tt_content']['types']['nj_carousel'] = array(
			'showitem' => '
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.general;general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.header;header,
		 nj_version,
		nj_slides,
--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:tabs.appearance,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.frames;frames,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:tabs.access,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.visibility;visibility,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.access;access,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:tabs.extended,
');
	 * 
	 * 
	 * 
	 * 
	 * 
	 'type' => 'inline',
            'foreign_table' => 'sys_file_reference',
            'foreign_field' => 'uid_foreign',
            'foreign_sortby' => 'sorting_foreign',
            'foreign_table_field' => 'tablenames',
            'foreign_match_fields' => [
                'fieldname' => $fieldName
            ],
            'foreign_label' => 'uid_local',
            'foreign_selector' => 'uid_local',
	 */

include_once('ce/list.php');