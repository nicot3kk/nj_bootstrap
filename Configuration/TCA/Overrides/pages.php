<?php
if (!defined ('TYPO3_MODE')) die ('Access denied.');

$nj_ext['lang']['be'] = 'LLL:EXT:nj_bootstrap/Resources/Private/Language/locallang_be.xlf:';
$nj_domain = 'pages';


$tcaPagesGeneral = [
	'nj_bg_image' => [
		'label' => 'Hintergrund-Bild',
		'l10n_mode' => 'mergeIfNotBlank',
		'exclude' => 1,
		'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
			'appearance' => array(
				'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference',
			),
			'foreign_match_fields' => array(
               'fieldname' => 'nj_bg_image',
            ),
			'minitems' => 0,
			'maxitems' => 1,
        ],
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
	],
	'nj_date' => [
		'label' => 'Datum',
		'l10n_mode' => 'mergeIfNotBlank',
		'exclude' => 1,
		'config' => [
			'type' => 'input',
			'size' => '13',
			'max' => '20',
			'eval' => 'date',
			'default' => '0'
		]
	],
	'nj_image' => [
		'label' => 'Bild',
		'l10n_mode' => 'mergeIfNotBlank',
		'exclude' => 1,
		'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
			'appearance' => array(
				'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference',
			),
			'foreign_match_fields' => array(
			   'fieldname' => 'nj_bg_image',
			),
			'minitems' => 0,
			'maxitems' => 1,
		],
		$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
	],
	'nj_teaser_image' => [
		'exclude' => 1,
		'label' => 'Teaser-Bild',
		'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
			'appearance' => array(
				'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference',
			),
			'foreign_match_fields' => array(
               'fieldname' => 'nj_teaser_image',
            ),
			'foreign_types' => array(
                \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
                   'showitem' => '--palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette,
                                  --palette--;;imageoverlayPalette,
                                  --palette--;;filePalette'
                ),
            ),
			'minitems' => 0,
			'maxitems' => 1,
        ],
        $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
	],
];


$tcaPagesInfos = [
//	'nj_og_info' => [
//		'exclude' => 1,
//		'config' => [
//			'type' => 'user',
//			'size' => '30',
//			'userFunc' => 'T3kk\NjBootstrap\Utility\TcaUtility->infoText',
//			'parameters' => [
//				'text' => $valid_ext['lang']['be'].'info.pages.ogInfo'
//			]
//		]
//	],
];


$tcaPagesStyle = [
	'nj_class' => [
		'label' => 'Css Klasse',
		'l10n_mode' => 'mergeIfNotBlank',
		'exclude' => 1,
		'config'  => [
			'type' => 'input',
			'size' => 25,
			'eval' => 'trim',
			'max'  => 256
		]
	],
	'nj_class_next_level' => [
		'label' => 'Css Klasse (für Unterseiten dieser Seite)', 
		'l10n_mode' => 'mergeIfNotBlank',
		'exclude' => 1,
		'config'  => [
			'type' => 'input',
			'size' => 25,
			'eval' => 'trim',
			'max'  => 256
		]
	],
];


$tcaPagesSeo = [
	'nj_seo_title' => [
		'exclude' => 1,
		'label' => 'Suchmaschinenoptimierter Seitentitel',
		'config'  => [
			'type' => 'input',
			'size' => 25,
			'eval' => 'trim',
			'max'  => 65
		]
	],
	'nj_og_title_alt' => [
		'exclude' => 1,
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => 'Alternativen Titel verwenden',
		'onChange' => 'reload',
		'config'  => [
			'type' => 'check',
			'default' => 0
		]
	],
	'nj_og_title' => [
		'displayCond' => 'FIELD:nj_og_title_alt:>=:1',
		'exclude' => 1,
		'label' => '',
		'config'  => [
			'type' => 'input',
			'size' => 25,
			'eval' => 'trim',
			'max'  => 256
		]
	],
	'nj_og_descr_alt' => [
		'exclude' => 1,
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => 'Alternate Beschreibung verwenden',
		'onChange' => 'reload',
		'config'  => [
			'type' => 'check',
			'default' => 0
		]
	],
	'nj_og_descr' => [
		'displayCond' => 'FIELD:nj_og_descr_alt:>=:1',
		'exclude' => 1,
		'label'   => '',
		'config'  => [
			'type' => 'text',
			'cols' => 60,
			'rows' => 6,
			'max' => 297
		]
	],
	'nj_og_image_alt' => [
		'exclude' => 1,
		'label' => 'Alternatives Bild verwenden',
		'onChange' => 'reload',
		'config'  => [
			'type' => 'check',
			'default' => 0
		]
	],
	'nj_og_image' => [
		'displayCond' => 'FIELD:nj_og_image_alt:>=:1',
		'exclude' => 1,
		'label' => '',
		'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
			'appearance' => [
				'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference',
				'enabledControls' => [
					'info' => TRUE,
					'new' => FALSE,
					'dragdrop' => TRUE,
					'sort' => TRUE,
					'hide' => TRUE,
					'delete' => TRUE,
					'localize' => FALSE,
				]
			],
			'minitems' => 0,
			'maxitems' => 1,
			'uploadfolder' => 'user_upload/general/images/openGraph',
		],
		$GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']),
    ]
];


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages',$tcaPagesGeneral);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages',$tcaPagesInfos);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages',$tcaPagesStyle);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages',$tcaPagesSeo);


$palettes = [
	'nj_class' => [
		'title' => '',
		'fields' => 'nj_class,nj_class_next_level'
	],
	'nj_og_title' => [
		'title' => 'og:title',
		'fields' => 'nj_og_title_alt,--linebreak--,nj_og_title'
	],
	
	'nj_og_description' => [
		'title' => 'og:description',
		'fields' => 'nj_og_descr_alt,--linebreak--,nj_og_descr'
	],
	'nj_og_image' => [
		'title' => 'og:image',
		'fields' => 'nj_og_image_alt,--linebreak--,nj_og_image'
	],
];


foreach($palettes as $key => $value) {
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
		'pages', $key, $value['fields']
	);
}


/**
 * Set available fields for extra page types
 */

$addFieldsPages = [
	'default' => '--div--;njs Bootstrap,'
		.'--palette--;;nj_class',
	'meta' => '--div--;SEO,'
		.'keywords,description,abstract,valid_og_info,'
		.'nj_seo_title,'
		.'--palette--;'.$palettes['nj_og_title']['title'].';nj_og_title,'
		.'--palette--;'.$palettes['nj_og_description']['title'].';nj_og_description,'
		.'--palette--;'.$palettes['nj_og_image']['title'].';nj_og_image',
];


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
	'pages',
	','.$addFieldsPages['meta'].','.$addFieldsPages['default']
);

$requestUpdateFields = 'nj_class_enable,'
	. 'nj_og_descr_alt,nj_og_image_alt,nj_og_title_alt';

$GLOBALS['TCA']['pages']['ctrl']['requestUpdate'] = $TCA['pages']['ctrl']['requestUpdate'] ? $TCA['pages']['ctrl']['requestUpdate'] . ',' . $requestUpdateFields : $requestUpdateFields;

/**
 * Set available fields for extra page types
 */
//\T3kk\NjBootstrap\Service\GeneralService::registerIcons(
//	\T3kk\NjBootstrap\Service\GeneralService::TABLE_PAGES
//);


