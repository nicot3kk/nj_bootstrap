<?php

$extKey		= 'nj_bootstrap';
$table		= 'sys_category';
$extLang	= 'LLL:EXT:nj_bootstrap/Resources/Private/Language/locallang_be.xlf:';
	

/**
 * default fields:
 * deleted,hidden,
 * header,subheader,bodytext,header_layout,layout,menu_type,sys_language_uid
 */

call_user_func(
	function ($extKey,$table,$extLang) {
	
		$tcaAddFields = [
			'nj_subtitle' => [
				'label' => $extLang.'tca.subtitle',
				'exclude' => 1,
				'config' => [
					'type' => 'input',
					'size' => 25,
					'max' => 65,
					'eval' => 'trim',
				]
			],
		];
		
		\T3kk\NjBootstrap\Utility\TcaUtility::addTCAcolumns($table, $tcaAddFields);
		\T3kk\NjBootstrap\Utility\TcaUtility::addTCAfields(
			$table, 
			'nj_subtitle',
			'after:title'
		);
	},
	$extKey,$table,$extLang
);
