<?php

$ce = 'form';

call_user_func(
	function ($ce,$extKey,$table,$extLang) {
// Add the FlexForm
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
			'*',
			'FILE:EXT:nj_bootstrap/Configuration/FlexForms/tx_njbootstrap_domain_model_form.xml',
			'nj_bootstrap_form'
		);
	},
	$ce,$extKey,$table,$extLang
);	

/**
 * Content element : nj_contact
 */
call_user_func(
	function ($ce,$extKey,$table,$extLang) {
	
		$tcaAddFields = [
			
		];
	
		addTCAcolumns($tcaAddFields);
		
	},
	$ce,$extKey,$table,$extLang
);

	
call_user_func(
	function ($ce,$extKey,$table,$extLang,$defaultContentFields) {
	
		/**
		 * Add own content elements
		 */
		$contentElement = [
			'types' => [
				'nj_'.$ce => [
					'showitem' => '--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,
					header,subheader,nj_menu_display,nj_menu_title,bodytext,nj_info_random_image,image,nj_image_filter_enable,nj_image_filter,'
						.$defaultContentFields['rendering']
				]
			],
			'columns' => array(
				'CType' => array(
					'config' => array(
						'items' => array(
							'nj_'.$ce => array(
								ucfirst($ce), // Name des Inhaltselementes
								'nj_'.$ce, // TCA Name des Inhaltselementes'
								'EXT:nj_bootstrap/Resources/Public/Icons/ce_'.$ce.'.svg' // Bild des Inhaltelementes
							)
						)
					)
				),
			)
		];
		
		addContentElement($contentElement);
		
	},
	$ce,$extKey,$table,$extLang,$defaultContentFields
);

	
	
	
	
/**
 * Columns overrides
 */
call_user_func(
	function ($ce,$extKey,$table,$extLang) {
	
		$GLOBALS['TCA']['tt_content']['types']['nj_'.$ce]['columnsOverrides'] = [
			
		];
		
	},
	$ce,$extKey,$table,$extLang
);