<?php

$ce = 'textimage';

/**
 * Content element : nj_textimage
 */
call_user_func(
	function ($ce,$extKey,$table,$extLang) {
	
		$columns = [
			
		];	
		addTCAcolumns($columns);
		
	},
	$ce,$extKey,$table,$extLang
);
	

call_user_func(
	function ($ce,$extKey,$table,$extLang,$contentFields) {
	
		/**
		 * Add own content elements
		 */
		$contentElement = [
			'types' => [
				'nj_'.$ce => [
					'showitem' => '--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,'
						.'header,subheader,nj_menu_display,nj_menu_title,layout,bodytext,nj_info_random_image,image,nj_image_filter_enable,nj_image_filter,'
						.'nj_load_assets,--palette--;;nj_animation,'
						.$contentFields['button']
				]
			],
			'columns' => [
				'CType' => addCtypeToColumns($ce)
			]
		];
		
		addContentElement($contentElement);
		
		/**
		 * Columns overrides
		 */
		
		$GLOBALS['TCA']['tt_content']['types']['nj_'.$ce]['columnsOverrides'] = [
			'bodytext' => [
				'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
			],
			'layout' => [
				'config' => [
					'type' => 'select',
					'renderType' => 'selectSingle',
					'items' => [
						['2 Spalten: Text Links / Bild Rechts', '0'],
						['2 Spalten: Bild Links / Text Rechts', '1'],
						['1 Spalte: Bild im Hintergrund / Text im Vordergrund', '2'],
						['1 Spalte: Bild oben / Text unten', '3'],
						['1 Spalte: Text oben / Bild unten', '4'],
					],
					'default' => 0
				],
			],
		];
		
	},
	$ce,$extKey,$table,$extLang,$defaultContentFields
);
	
/**
 * Columns overrides
 */
call_user_func(
	function ($ce,$extKey,$table,$extLang) {
	
		$GLOBALS['TCA']['tt_content']['types']['nj_'.$ce]['columnsOverrides'] = [
			'bodytext' => [
				'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
			],
		];
		
	},
	$ce,$extKey,$table,$extLang
);