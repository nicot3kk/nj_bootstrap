<?php

$ce = 'hero';

/**
 * Content element : nj_hero
 */
call_user_func(
	function ($ce,$extKey,$table,$extLang) {
	
		$tcaAddFields = [
			
		];
	
		addTCAcolumns($tcaAddFields);
		
	},
	$ce,$extKey,$table,$extLang
);

	
call_user_func(
	function ($ce,$extKey,$table,$extLang) {
	
		/**
		 * Add own content elements
		 */
		$contentElement = [
			'types' => [
				'nj_'.$ce => [
					'showitem' => '--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,'
						.'header,subheader,nj_menu_display,nj_menu_title,bodytext,nj_image,nj_bg_image,nj_info_random_image,image,nj_image_filter_enable,nj_image_filter,'
				]
			],
			'columns' => array(
				'CType' => array(
					'config' => array(
						'items' => array(
							'nj_'.$ce => array(
								ucfirst($ce), // Name des Inhaltselementes
								'nj_'.$ce, // TCA Name des Inhaltselementes'
								'EXT:nj_bootstrap/Resources/Public/Icons/ce_'.$ce.'.svg' // Bild des Inhaltelementes
							)
						)
					)
				),
			)
		];
		
		addContentElement($contentElement);
		
	},
	$ce,$extKey,$table,$extLang
);

	
/**
 * Columns overrides
 */
call_user_func(
	function ($ce,$extKey,$table,$extLang) {
	
		$GLOBALS['TCA']['tt_content']['types']['nj_'.$ce]['columnsOverrides'] = [
			'nj_image' => [
				'label' => 'Logo',
				'config' => [
					'foreign_selector_fieldTcaOverride' => [
						'config' => [
							'appearance' => [
								'elementBrowserType' => 'file',
								'elementBrowserAllowed' => 'svg'
							]
						]
					],
					'maxitems' => 1,
					'filter' => [
						[
							'parameters' => [
								'allowedFileExtensions' => 'svg',
								'disallowedFileExtensions' => 'jpg,jpeg,png,gif,tif,tiff,bmp,png' 
							]
						]
					]
				]
			],
		];
		
	},
	$ce,$extKey,$table,$extLang
);