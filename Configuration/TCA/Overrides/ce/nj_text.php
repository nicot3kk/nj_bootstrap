<?php

$ce = 'text';

/**
 * Content element : nj_text
 */
call_user_func(
	function ($ce,$extKey,$table,$extLang) {
	
		$tcaAddFields = [
			
		];
	
		addTCAcolumns($tcaAddFields);
		
	},
	$ce,$extKey,$table,$extLang
);

	
call_user_func(
	function ($ce,$extKey,$table,$extLang,$defaultContentFields) {
	
		/**
		 * Add own content elements
		 */
		$contentElement = [
			'types' => [
				'nj_'.$ce => [
					'showitem' => '--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,'
						.'header,subheader,nj_menu_display,nj_menu_title,bodytext,'
						.$defaultContentFields['rendering']
						.$defaultContentFields['style']
				]
			],
			'columns' => array(
				'CType' => array(
					'config' => array(
						'items' => array(
							'nj_'.$ce => array(
								ucfirst($ce), // Name des Inhaltselementes
								'nj_'.$ce, // TCA Name des Inhaltselementes'
								'EXT:nj_bootstrap/Resources/Public/Icons/ce_'.$ce.'.svg' // Bild des Inhaltelementes
							)
						)
					)
				),
			)
		];
		
		addContentElement($contentElement);
		
	},
	$ce,$extKey,$table,$extLang,$defaultContentFields
);

	
/**
 * Columns overrides
 */
call_user_func(
	function ($ce,$extKey,$table,$extLang) {
	
		$GLOBALS['TCA']['tt_content']['types']['nj_'.$ce]['columnsOverrides'] = [
			'bodytext' => [
				'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
			],
		];
		
	},
	$ce,$extKey,$table,$extLang
);