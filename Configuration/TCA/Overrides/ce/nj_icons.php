<?php

$ce = 'icons';

/**
 * Content element : nj_icons
 */
call_user_func(
	function ($ce,$extKey,$table,$extLang) {
	
		$tcaAddFields = [
			'nj_icons' => [
				'displayCond' => 'FIELD:sys_language_uid:<=:0',
				'exclude'	=> 1,
				'label'		=> 'Icons',
				'config' => [
					'type' => 'inline',
					'foreign_table' => 'tx_njbootstrap_domain_model_icon',
					'foreign_table_field' => 'foreign_table',
					'foreign_field' => 'foreign_uid',
					'foreign_sortby' => 'foreign_sorting',
					'foreign_record_defaults' => [
						'foreign_field' => 'nj_icons'
					 ],
					'foreign_match_fields' => [
						'foreign_field' => 'nj_icons'
					],
					'maxitems' => 999,
					'appearance' => [
						'collapseAll' => 1,
						'levelLinksPosition' => 'top',
						'showSynchronizationLink' => 1,
						'showPossibleLocalizationRecords' => 1,
						'showAllLocalizationLink' => 1
					],
				],
			],
		];
	
		addTCAcolumns($tcaAddFields);
		
	},
	$ce,$extKey,$table,$extLang
);

	
call_user_func(
	function ($ce,$extKey,$table,$extLang,$defaultContentFields) {
	
		/**
		 * Add own content elements
		 */
		$contentElement = [
			'types' => [
				'nj_'.$ce => [
					'showitem' => '--palette--;LLL:EXT:cms/locallang_ttc.xlf:palette.general;general,'
						.'header,subheader,nj_menu_display,nj_menu_title,bodytext,'
						.'--div--;'.$extLang.'tab.icons,'
						.'nj_icons,'
						.$defaultContentFields['style']
				]
			],
			'columns' => array(
				'CType' => array(
					'config' => array(
						'items' => array(
							'nj_'.$ce => array(
								ucfirst($ce), // Name des Inhaltselementes
								'nj_'.$ce, // TCA Name des Inhaltselementes'
								'EXT:nj_bootstrap/Resources/Public/Icons/ce_'.$ce.'.svg' // Bild des Inhaltelementes
							)
						)
					)
				),
			)
		];
		
		addContentElement($contentElement);
		
	},
	$ce,$extKey,$table,$extLang,$defaultContentFields
);

	
/**
 * Columns overrides
 */
call_user_func(
	function ($ce,$extKey,$table,$extLang) {
	
		$GLOBALS['TCA']['tt_content']['types']['nj_'.$ce]['columnsOverrides'] = [
			
		];
		
	},
	$ce,$extKey,$table,$extLang
);