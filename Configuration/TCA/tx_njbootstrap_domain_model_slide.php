<?php
if (!defined('TYPO3_MODE')) { die('Access denied.'); }

$ext = 'tx_njbootstrap';
$extKey = 'nj_bootstrap';
$model = 'slide';
$langFile = 'LLL:EXT:nj_bootstrap/Resources/Private/Language/locallang_be.xlf:';
$allowedFileExtensions = 'jpg,jpeg,png,svg';

return [
	'ctrl' => array(
		'crdate' => 'crdate',
		'default_sortby' => 'ORDER BY title ASC',
		'delete' => 'deleted',
		'dividers2tabs' => TRUE,
		'enablecolumns' => array(
			'disabled' => 'hidden'
		),
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey) . 'Resources/Public/Icons/' . $ext . '_domain_model_' . $model . '.svg',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => 'title',
		'languageField' => 'sys_language_uid',
		'origUid' => 't3_origuid',
		'requestUpdate' => 'sys_language_uid,background',
		//'sortby' => 'sorting',
		'title' => $langFile.'model.'.$model,
		'transOrigDiffSourceField' => 'l18n_diffsource',
		'transOrigPointerField' => 'l18n_parent',
		'tstamp' => 'tstamp',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
	),
	'interface' => [
		'showRecordFieldList' => 'hidden,'
	],
	'columns' => [
        'foreign_uid' => [
            'label' => 'LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.uid_foreign',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'eval' => 'int'
            ]
        ],
        'foreign_table' => [
            'label' => 'LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.tablenames',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ]
        ],
        'foreign_field' => [
            'label' => 'LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.fieldname',
            'config' => [
                'type' => 'input',
                'size' => 30
            ]
        ],
        'foreign_sorting' => [
            'label' => 'LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.sorting_foreign',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'max' => 4,
                'eval' => 'int',
                'range' => [
                    'upper' => '1000',
                    'lower' => '10'
                ],
                'default' => 0
            ]
        ],
		'content' => [
			'exclude' => 1,
			'label'   => $langFile.'tca.text',
			'config'  => [
				'type' => 'text',
				'cols' => 60,
				'max' => 256,
				'rows' => 6,
			]
		],
		'image' => [
			'displayCond' => [
				'AND' => [
					'FIELD:sys_language_uid:<=:0',
					'FIELD:slide_type:=:1'
				]
			],
			'exclude' => 1,
			'l10n_mode' => 'exclude',
			'label' => $langFile.'tca.backgroundImage',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
				'appearance' => array(
					'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference',
				),
				'foreign_match_fields' => array(
				   'fieldname' => 'valid_bg_image',
				),
				'minitems' => 0,
				'maxitems' => 1,
			],
			$allowedFileExtensions),
		],
		'subtitle' => [
			'label' => $langFile.'tca.subtitle',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 40,
				'max' => 125,
				'eval' => 'trim',
			]
		],
		'title' => [
			'label' => $langFile.'tca.headline',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 65,
				'eval' => 'trim',
			]
		],
		'slide_type' => [
			'displayCond' => 'FIELD:sys_language_uid:<=:0',
			'l10nmode' => 'mergeIfNotBlank',
			'exlude' => 1,
			'label'   => $langFile.'tca.slideType',
			'config'  => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					[$langFile.'tca.backgroundImage',1],
					[$langFile.'tca.video', 2]
				],
			]
		],
	],
	'types' => [
		'1' => [
			'showitem' => 'hidden, title,subtitle,content,slide_type,image,video_id, --palette--;;1',
		],
	],
	'palettes' => [
		'1' => [
			'showitem' => '',
		],
	]
];