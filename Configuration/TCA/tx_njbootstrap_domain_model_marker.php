<?php
if (!defined('TYPO3_MODE')) { die('Access denied.'); }

$ext = 'tx_njbootstrap';
$extKey = 'nj_bootstrap';
$model = 'marker';
$langFile = 'LLL:EXT:nj_bootstrap/Resources/Private/Language/locallang_be.xlf:';
$allowedFileExtensions = 'jpg,jpeg,png,svg';

return [
	'ctrl' => [
		'crdate' => 'crdate',
		'default_sortby' => 'ORDER BY title ASC',
		'delete' => 'deleted',
		'dividers2tabs' => TRUE,
		'enablecolumns' => array(
			'disabled' => 'hidden'
		),
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey) . 'Resources/Public/Icons/' . $ext . '_domain_model_' . $model . '.svg',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => 'title',
		'languageField' => 'sys_language_uid',
		'origUid' => 't3_origuid',
		'requestUpdate' => 'sys_language_uid,background',
		//'sortby' => 'sorting',
		'title' => $langFile.'model.'.$model,
		'transOrigDiffSourceField' => 'l18n_diffsource',
		'transOrigPointerField' => 'l18n_parent',
		'tstamp' => 'tstamp',
		'versioningWS' => 2,
		'versioning_followPages' => TRUE,
	],
	'interface' => [
		'showRecordFieldList' => 'hidden,'
	],
	'columns' => [
        'foreign_uid' => [
            'label' => 'LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.uid_foreign',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'eval' => 'int'
            ]
        ],
        'foreign_table' => [
            'label' => 'LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.tablenames',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ]
        ],
        'foreign_field' => [
            'label' => 'LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.fieldname',
            'config' => [
                'type' => 'input',
                'size' => 30
            ]
        ],
        'foreign_sorting' => [
            'label' => 'LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.sorting_foreign',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'max' => 4,
                'eval' => 'int',
                'range' => [
                    'upper' => '1000',
                    'lower' => '10'
                ],
                'default' => 0
            ]
        ],
		'city' => [
			'label' => 'City',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 50,
				'eval' => 'trim',
			]
		],
		'country' => [
			'label' => 'Country',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 50,
				'eval' => 'trim',
			]
		],
		'geo_location' => [
			'displayCond' => 'FIELD:sys_language_uid:<=:0',
			'exclude' => 1,
			'label' => '',
			'config' => [
				'type' => 'user',
				'userFunc' => 'T3kk\NjBootstrap\Utility\TcaUtility->geoLocation',
				'parameters' => []
			]
		], 
		'latitude' => [
			'label' => 'Latitude',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 50,
				'eval' => 'trim',
			]
		],
		'longitude' => [
			'label' => 'Longitude',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 50,
				'eval' => 'trim',
			]
		],
		'street' => [
			'label' => 'Street',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 255,
				'eval' => 'trim',
			]
		],
		'title' => [
			'label' => 'Title',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 255,
				'eval' => 'trim',
			]
		],
		'zip_code' => [
			'label' => 'PLZ',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 5,
				'max' => 7,
				'eval' => 'trim',
			]
		],
	],
	'types' => [
		'1' => [
			'showitem' => 'hidden,title,street,zip_code,city,country,geo_location,latitude,longitude',
		],
	],
	'palettes' => [
		'1' => [
			'showitem' => '',
		],
	]
];