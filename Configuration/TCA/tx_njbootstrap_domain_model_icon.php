<?php
if (!defined('TYPO3_MODE')) { die('Access denied.'); }

$ext = 'tx_njbootstrap';
$extKey = 'nj_bootstrap';
$model = 'icon';
$langFile = 'LLL:EXT:nj_bootstrap/Resources/Private/Language/locallang_be.xlf:';
$allowedFileExtensions = 'svg';

// Add an extra categories selection field to the fe_users table
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
	$ext,
	$ext.'_domain_model_'.$model,
	// Do not use the default field name ("categories"), which is already used
	'categories',
	[
		// Set a custom label
		'label' => 'Kategorien',
		// This field should not be an exclude-field
		'exclude' => 0,
		// Override generic configuration, e.g. sort by title rather than by sorting
		'fieldConfiguration' => array(
				'foreign_table_where' => ' AND sys_category.sys_language_uid IN (-1, 0) ORDER BY sys_category.title ASC',
		),
		// string (keyword), see TCA reference for details
		'l10n_mode' => 'exclude',
		// list of keywords, see TCA reference for details
		'l10n_display' => 'hideDiff',
	]
);


return [
	'ctrl' => array(
		'crdate' => 'crdate',
		'default_sortby' => 'ORDER BY title ASC',
		'delete' => 'deleted',
		'dividers2tabs' => TRUE,
		'enablecolumns' => array(
			'disabled' => 'hidden'
		),
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey) . 'Resources/Public/Icons/' . $ext . '_domain_model_' . $model . '.svg',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => 'title',
		'languageField' => 'sys_language_uid',
		'requestUpdate' => 'sys_language_uid,background',
		//'sortby' => 'sorting',
		'title' => $langFile.'model.'.$model,
		'transOrigDiffSourceField' => 'l18n_diffsource',
		'transOrigPointerField' => 'l18n_parent',
		'tstamp' => 'tstamp',
	),
	'interface' => [
		'showRecordFieldList' => 'hidden,'
	],
	'columns' => [
        'foreign_uid' => [
            'label' => 'LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.uid_foreign',
            'config' => [
                'type' => 'input',
                'size' => 10,
                'eval' => 'int'
            ]
        ],
        'foreign_table' => [
            'label' => 'LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.tablenames',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ]
        ],
        'foreign_field' => [
            'label' => 'LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.fieldname',
            'config' => [
                'type' => 'input',
                'size' => 30
            ]
        ],
        'foreign_sorting' => [
            'label' => 'LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.sorting_foreign',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'max' => 4,
                'eval' => 'int',
                'range' => [
                    'upper' => '1000',
                    'lower' => '10'
                ],
                'default' => 0
            ]
        ],
		'content' => [
			'exclude' => 1,
			'label'   => $langFile.'tca.text',
			'config'  => [
				'type' => 'text',
				'cols' => 60,
				'max' => 256,
				'rows' => 6,
			]
		],
		'svg_file' => [
			'displayCond' => 'FIELD:sys_language_uid:<=:0',
			'exclude' => 1,
			'l10n_mode' => 'exclude',
			'label' => 'Icon file',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('image', [
				'appearance' => array(
					'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference',
				),
				'foreign_match_fields' => array(
				   'fieldname' => 'svg_file',
				),
				'minitems' => 0,
				'maxitems' => 1,
			],
			$allowedFileExtensions),
		],
		'svg_inline' => [
			'exclude' => 1,
			'label'   => 'SVG inline',
			'config'  => [
				'type' => 'text',
				'cols' => 60,
				'rows' => 12,
			]
		],
		'title' => [
			'label' => $langFile.'tca.headline',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 65,
				'eval' => 'trim',
			]
		],
	],
	'types' => [
		'1' => [
			'showitem' => 'hidden,title,category,content,svg_file,svg_inline,',
		],
	],
	'palettes' => [
		'1' => [
			'showitem' => '',
		],
	]
];