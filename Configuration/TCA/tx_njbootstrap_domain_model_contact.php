<?php
if (!defined('TYPO3_MODE')) { die('Access denied.'); }

$model = 'contact';

$ext = 'tx_njbootstrap';
$extKey = 'nj_bootstrap';
$langFile = 'LLL:EXT:nj_bootstrap/Resources/Private/Language/locallang_be.xlf:';
$allowedFileExtensions = 'svg';

return [
	'ctrl' => [
		'crdate' => 'crdate',
		'default_sortby' => 'ORDER BY title ASC',
		'delete' => 'deleted',
		'dividers2tabs' => TRUE,
		'enablecolumns' => [
			'disabled' => 'hidden'
		],
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($extKey) . 'Resources/Public/Icons/' . $ext . '_domain_model_' . $model . '.svg',
		'l10n_mode' => 'mergeIfNotBlank',
		'label' => 'title',
		'languageField' => 'sys_language_uid',
		'requestUpdate' => 'sys_language_uid',
		//'sortby' => 'sorting',
		'title' => $langFile.'model.'.$model,
		'transOrigDiffSourceField' => 'l18n_diffsource',
		'transOrigPointerField' => 'l18n_parent',
		'tstamp' => 'tstamp',
	],
	'interface' => [
		'showRecordFieldList' => 'hidden,'
	],
	'columns' => [
		'city' => [
			'label' => 'City',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 50,
				'eval' => 'trim',
			]
		],
		'country' => [
			'label' => 'Country',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 50,
				'eval' => 'trim',
			]
		],
		'email' => [
			'label' => 'E-Mail-Adresse',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 255,
				'eval' => 'trim',
			]
		],
		'firm' => [
			'label' => 'Firm',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 255,
				'eval' => 'trim',
			]
		],
		'street' => [
			'label' => 'Street',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 255,
				'eval' => 'trim',
			]
		],
		'telephone' => [
			'label' => 'Telephone',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 255,
				'eval' => 'trim',
			]
		],
		'title' => [
			'label' => 'Title',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 25,
				'max' => 255,
				'eval' => 'trim',
			]
		],
		'zip_code' => [
			'label' => 'PLZ',
			'exclude' => 1,
			'config' => [
				'type' => 'input',
				'size' => 5,
				'max' => 7,
				'eval' => 'trim',
			]
		],
	],
	'types' => [
		'1' => [
			'showitem' => 'hidden,title,firm,street,zip_code,city,country,telephone,telefax,mobile,website,facebook,twitter,xing,google_plus',
		],
	],
	'palettes' => [
		'1' => [
			'showitem' => '',
		],
	]
];