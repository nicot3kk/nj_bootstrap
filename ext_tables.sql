#  
# Additional fields for the 'pages' table
#

CREATE TABLE pages (
	nj_bg_image int(11) unsigned DEFAULT '0' NOT NULL,
	nj_class varchar(255) DEFAULT '' NOT NULL,
	nj_class_next_level varchar(255) DEFAULT '' NOT NULL,
	nj_date varchar(25) DEFAULT '' NOT NULL,
	nj_og_descr_alt tinyint(4) DEFAULT '0' NOT NULL,
	nj_image int(11) unsigned DEFAULT '0' NOT NULL,
	nj_og_descr text NOT NULL,
	nj_og_image_alt tinyint(4) DEFAULT '0' NOT NULL,
	nj_og_image int(11) unsigned DEFAULT '0' NOT NULL,
	nj_og_title_alt tinyint(4) DEFAULT '0' NOT NULL,
	nj_og_title varchar(255) DEFAULT '' NOT NULL,
	nj_seo_title varchar(255) DEFAULT '' NOT NULL,
	nj_teaser_image int(11) unsigned DEFAULT '0' NOT NULL,
);

CREATE TABLE pages_language_overlay (
	nj_og_descr text NOT NULL,
	nj_og_title varchar(255) DEFAULT '' NOT NULL,
	nj_seo_title varchar(255) DEFAULT '' NOT NULL,
);

#  
# Additional field for the 'tt_content' table
#

CREATE TABLE tt_content (
	nj_add_html_inline text NOT NULL,
	nj_add_html_file varchar(255) DEFAULT '' NOT NULL,
	nj_add_html_use tinyint(4) DEFAULT '1' NOT NULL,
	nj_alignment varchar(25) DEFAULT '' NOT NULL,
	nj_animation varchar(25) DEFAULT '' NOT NULL,
	nj_animation_enable tinyint(4) DEFAULT '0' NOT NULL,
	nj_autoplay tinyint(4) DEFAULT '1' NOT NULL,
	nj_bg_image int(11) unsigned DEFAULT '0' NOT NULL,
	nj_btn_action varchar(255) DEFAULT '' NOT NULL,
	nj_btn_action_data varchar(255) DEFAULT '' NOT NULL,
	nj_btn_class varchar(255) DEFAULT '' NOT NULL,
	nj_btn_is_ajax tinyint(4) DEFAULT '0' NOT NULL,
	nj_btn_link varchar(255) DEFAULT '' NOT NULL,
	nj_btn_label varchar(50) DEFAULT '' NOT NULL,
	nj_btn_type tinyint(4) DEFAULT '0' NOT NULL,
	nj_btn_use tinyint(4) DEFAULT '0' NOT NULL,
	nj_color_sheme varchar(50) DEFAULT '' NOT NULL,
	nj_css_classes varchar(255) DEFAULT '' NOT NULL,
	nj_css_inline varchar(255) DEFAULT '' NOT NULL,
	nj_easing varchar(50) DEFAULT '' NOT NULL,
	nj_effect varchar(50) DEFAULT '' NOT NULL,
	nj_direction_next varchar(25) DEFAULT '' NOT NULL,
	nj_direction_prev varchar(25) DEFAULT '' NOT NULL,
	nj_icons int(11) DEFAULT '0' NOT NULL,
	nj_image int(11) unsigned DEFAULT '0' NOT NULL,
	nj_image_filter varchar(25) DEFAULT '' NOT NULL,
	nj_image_filter_enable tinyint(4) DEFAULT '0' NOT NULL,
	nj_gm_api_key varchar(50) DEFAULT '' NOT NULL,
	nj_gm_marker varchar(255) DEFAULT '' NOT NULL,
	nj_menu_display tinyint(4) DEFAULT '1' NOT NULL,
	nj_menu_title varchar(25) DEFAULT '' NOT NULL,
	nj_pause_on_hover tinyint(4) DEFAULT '1' NOT NULL,
	nj_render_type varchar(25) DEFAULT '' NOT NULL,
	nj_render_version varchar(25) DEFAULT '' NOT NULL,
	nj_singular_content tinyint(4) DEFAULT '0' NOT NULL,
	nj_slides varchar(255) DEFAULT '' NOT NULL,
	nj_time_effect int(11) DEFAULT '125' NOT NULL,
	nj_time_show int(11) DEFAULT '5000' NOT NULL,
	nj_version varchar(25) DEFAULT '' NOT NULL,
);

CREATE TABLE sys_category (
	nj_subtitle varchar(255) DEFAULT '' NOT NULL,
);


#
# Table structure for table 'tx_njbootstrap_domain_model_contact'
#

CREATE TABLE tx_njbootstrap_domain_model_contact (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
    crdate int(11) unsigned DEFAULT '0' NOT NULL,
    cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
    fe_cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
    deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
    hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
    
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
    endtime int(11) unsigned DEFAULT '0' NOT NULL,
     
	city varchar(50) DEFAULT '' NOT NULL,
	country varchar(50) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	firm varchar(50) DEFAULT '' NOT NULL,
	mobile varchar(25) DEFAULT '' NOT NULL,
	street varchar(255) DEFAULT '' NOT NULL,
	telefax varchar(25) DEFAULT '' NOT NULL,
	telephone varchar(25) DEFAULT '' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	website varchar(255) DEFAULT '' NOT NULL,
	zip_code varchar(25) DEFAULT '' NOT NULL,

	facebook varchar(25) DEFAULT '' NOT NULL,
	google_plus varchar(25) DEFAULT '' NOT NULL,
	twitter varchar(25) DEFAULT '' NOT NULL,
	xing varchar(25) DEFAULT '' NOT NULL,

	# Language fields
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
    l18n_parent int(11) DEFAULT '0' NOT NULL,
    l18n_diffsource mediumblob NOT NULL,

    PRIMARY KEY (uid)
);

#
# Table structure for table 'tx_njbootstrap_domain_model_icon'
#

CREATE TABLE tx_njbootstrap_domain_model_icon (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
    crdate int(11) unsigned DEFAULT '0' NOT NULL,
    cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
    fe_cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
    deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
    hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
    
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
    endtime int(11) unsigned DEFAULT '0' NOT NULL,
     
	categories int(11) NOT NULL default '0',
	content text NOT NULL,
	svg_file int(11) unsigned DEFAULT '0' NOT NULL,
	svg_inline text NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,

	# Reference fields (basically same as MM table)
	foreign_uid int(11) DEFAULT '0' NOT NULL,
	foreign_table varchar(64) DEFAULT '' NOT NULL,
	foreign_field varchar(64) DEFAULT '' NOT NULL,
	foreign_sorting int(11) DEFAULT '0' NOT NULL,

	# Language fields
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
    l18n_parent int(11) DEFAULT '0' NOT NULL,
    l18n_diffsource mediumblob NOT NULL,

    PRIMARY KEY (uid),
	KEY parent (pid,deleted),
	KEY foreign_table_foreign_field (foreign_table(32),foreign_field(12)),
	KEY deleted (deleted),
	KEY foreign_uid (foreign_uid)
);

CREATE TABLE tx_njbootstrap_domain_model_slide (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
    crdate int(11) unsigned DEFAULT '0' NOT NULL,
    cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
    fe_cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
    deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
    hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
    
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
    endtime int(11) unsigned DEFAULT '0' NOT NULL,
     
	content text NOT NULL,
	image int(11) unsigned DEFAULT '0' NOT NULL,
	slide_type int(11) DEFAULT '1' NOT NULL,
	subtitle varchar(255) DEFAULT '' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	video_id varchar(255) DEFAULT '' NOT NULL,

	# Reference fields (basically same as MM table)
	foreign_uid int(11) DEFAULT '0' NOT NULL,
	foreign_table varchar(64) DEFAULT '' NOT NULL,
	foreign_field varchar(64) DEFAULT '' NOT NULL,
	foreign_sorting int(11) DEFAULT '0' NOT NULL,

	# Language fields
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
    l18n_parent int(11) DEFAULT '0' NOT NULL,
    l18n_diffsource mediumblob NOT NULL,

	# Versioning fields
	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(30) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,
	t3_origuid int(11) DEFAULT '0' NOT NULL,

    PRIMARY KEY (uid),
	KEY parent (pid,deleted),
	KEY foreign_table_foreign_field (foreign_table(32),foreign_field(12)),
	KEY deleted (deleted),
	KEY foreign_uid (foreign_uid)
);


#
# Table structure for table 'tx_njbootstrap_domain_model_marker'
#

CREATE TABLE tx_njbootstrap_domain_model_marker (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
    crdate int(11) unsigned DEFAULT '0' NOT NULL,
    cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
    fe_cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
    deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
    hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
    
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
    endtime int(11) unsigned DEFAULT '0' NOT NULL,
    
	# Language fields
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
    l18n_parent int(11) DEFAULT '0' NOT NULL,
    l18n_diffsource mediumblob NOT NULL,

	# Reference fields (basically same as MM table)
	foreign_uid int(11) DEFAULT '0' NOT NULL,
	foreign_table varchar(64) DEFAULT '' NOT NULL,
	foreign_field varchar(64) DEFAULT '' NOT NULL,
	foreign_sorting int(11) DEFAULT '0' NOT NULL,

	city varchar(50) DEFAULT '' NOT NULL,
	country varchar(50) DEFAULT '' NOT NULL,
	latitude varchar(50) DEFAULT '' NOT NULL,
	longitude varchar(50) DEFAULT '' NOT NULL,
	street varchar(255) DEFAULT '' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	zip_code varchar(25) DEFAULT '' NOT NULL,

	# Versioning fields
	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(30) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,
	t3_origuid int(11) DEFAULT '0' NOT NULL,


    PRIMARY KEY (uid),
	KEY parent (pid,deleted),
	KEY foreign_table_foreign_field (foreign_table(32),foreign_field(12)),
	KEY deleted (deleted),
	KEY foreign_uid (foreign_uid)
);


#
# Table structure for table 'tx_vdculturecheck_domain_model_form'
#

CREATE TABLE tx_njbootstrap_domain_model_form (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,

	country varchar(25) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	firm varchar(50) DEFAULT '' NOT NULL,
	firstName varchar(25) DEFAULT '' NOT NULL,
	lastName varchar(25) DEFAULT '' NOT NULL,
	location varchar(25) DEFAULT '' NOT NULL,
	message text NOT NULL,
	mobile varchar(25) DEFAULT '' NOT NULL,
	name varchar(50) DEFAULT '' NOT NULL,
	phone varchar(25) DEFAULT '' NOT NULL,
	street varchar(50) DEFAULT '' NOT NULL,
	subject varchar(125) DEFAULT '' NOT NULL,
	zipCode int(5) DEFAULT '0' NOT NULL,
	url varchar(255) DEFAULT '' NOT NULL,

	log_ip varchar(25) DEFAULT '' NOT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	
);